//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DateTimeStart { get; set; }
        public Nullable<System.DateTime> DateTimeEnd { get; set; }
        public Nullable<System.DateTime> DateTimeCreate { get; set; }
        public Nullable<int> LoginAccountCheckin { get; set; }
        public Nullable<int> LoginAccountCheckout { get; set; }
        public Nullable<int> LoginAccountCreate { get; set; }
        public string Payments { get; set; }
        public Nullable<double> Promotion { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public Nullable<double> TotalPayment { get; set; }
        public string Note { get; set; }
        public string Value01 { get; set; }
        public string Value02 { get; set; }
        public string Value03 { get; set; }
        public string Value04 { get; set; }
        public string Value05 { get; set; }
        public Nullable<int> Status { get; set; }
    }
}
