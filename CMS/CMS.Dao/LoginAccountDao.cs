﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Model;
using System.Linq.Expressions;

namespace CMS.Dao
{
    public class LoginAccountDao
    {
        QLyHoCauEntities _context = new QLyHoCauEntities();

        #region Kiểm tra tên đăng nhập
        /// <summary>
        /// Kiểm tra tên đăng nhập
        /// </summary>
        /// <param name="UserName">Tên đăng nhập</param>
        /// <returns></returns>
        public bool LoginAccount_CheckUserName(string UserName)
        {
            if (_context.LoginAccounts.Any(la => la.UserName.Trim() == UserName.Trim()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Kiểm tra đăng nhập tài khoản qua tên đăng nhập và mật khẩu
        /// <summary>
        /// Kiểm tra đăng nhập tài khoản qua tên đăng nhập và mật khẩu
        /// </summary>
        /// <param name="UserName">Tên đăng nhập</param>
        /// <param name="Password">Mật khẩu</param>
        /// <returns></returns>
        public bool LoginAccount_CheckLogin(string UserName, string Password)
        {
            string PasswordEncrypt = CMS.Common.StringProcess.Encryption.Md5Encrypt(Password);

            if (_context.LoginAccounts.Any(la => la.UserName.Trim() == UserName.Trim() && la.Password == PasswordEncrypt))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Lấy thông tin tài khoản thông qua UserName và Password
        /// <summary>
        /// Lấy thông tin tài khoản thông qua UserName và Password
        /// </summary>
        /// <param name="UserName">Tên đăng nhập</param>
        /// <param name="Password">Mật khẩu</param>
        /// <returns></returns>
        public LoginAccount LoginAccount_GetByUserNameAndPassword(string UserName, string Password)
        {
            string PasswordEncrypt = CMS.Common.StringProcess.Encryption.Md5Encrypt(Password);
            LoginAccount LoginAccountDetail;

            LoginAccountDetail = _context.LoginAccounts.First(la => la.UserName == UserName && la.Password == PasswordEncrypt);
            return LoginAccountDetail;
        }
        #endregion
        #region Cập nhật thời gian của lần đăng nhập cuối cùng
        /// <summary>
        /// Cập nhật thời gian của lần đăng nhập cuối cùng
        /// </summary>
        /// <param name="AccountId">Id tài khoản</param>
        /// <param name="LastLogin">Thời gian của lần đăng nhập cuối dùng</param>
        public void LoginAccount_UpdateLastLogin(int AccountId, DateTime LastLogin)
        {
            LoginAccount LoginAccountDetail = new LoginAccount()
            {
                Id = AccountId,
                LastLogin = LastLogin
            };

            using (QLyHoCauEntities DB = new QLyHoCauEntities())
            {
                DB.LoginAccounts.Attach(LoginAccountDetail);
                DB.Entry(LoginAccountDetail).Property(la => la.LastLogin).IsModified = true;
                DB.SaveChanges();
            }
        }
        #endregion
        #region Cập nhật trạng thái
        /// <summary>
        /// Cập nhật trạng thái
        /// </summary>
        /// <param name="AccountId">Id tài khoản</param>
        /// <param name="status">Trạng thái tài khoản</param>
        public void LoginAccount_UpdateStatus(int accountId, int status)
        {
            LoginAccount loginAccountDetail = new LoginAccount()
            {
                Id = accountId,
                Status = status
            };

            using (QLyHoCauEntities DB = new QLyHoCauEntities())
            {
                DB.LoginAccounts.Attach(loginAccountDetail);
                DB.Entry(loginAccountDetail).Property(la => la.Status).IsModified = true;
                DB.SaveChanges();
            }
        }
        #endregion
        #region Cập nhật thông tin tài khoản đăng nhập
        /// <summary>
        /// Cập nhật thông tin tài khoản đăng nhập
        /// </summary>
        /// <param name="LoginAccountDetail">Thông tin tài khoản đăng nhập</param>
        public void LoginAccount_Update(LoginAccount LoginAccountDetail)
        {
            _context.LoginAccounts.Attach(LoginAccountDetail);
            _context.Entry(LoginAccountDetail).Property(la => la.GroupAccountId).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.FullName).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.Phone).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.Email).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.Address).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.LastUpdate).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.AgencyId).IsModified = true;
            _context.SaveChanges();
        }
        #endregion
        #region Cập nhật thông tin tài khoản đăng nhập
        /// <summary>
        /// Cập nhật thông tin tài khoản đăng nhập
        /// </summary>
        /// <param name="LoginAccountDetail">Thông tin tài khoản đăng nhập</param>
        public void LoginAccount_UpdateDetail(LoginAccount LoginAccountDetail)
        {
            _context.LoginAccounts.Attach(LoginAccountDetail);
            _context.Entry(LoginAccountDetail).Property(la => la.FullName).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.Phone).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.Email).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.Address).IsModified = true;
            _context.Entry(LoginAccountDetail).Property(la => la.LastUpdate).IsModified = true;
            _context.SaveChanges();
        }
        #endregion
        #region Thêm mới tài khoản bằng PROCEDURE
        /// <summary>
        /// Thêm mới tài khoản bằng PROCEDURE
        /// </summary>
        /// <param name="LoginAccountDetail">Thông tin tài khoản đăng nhập</param>
        public void LoginAccount_Insert(LoginAccount LoginAccountDetail)
        {
            LoginAccountDetail.Password = CMS.Common.StringProcess.Encryption.Md5Encrypt(LoginAccountDetail.Password);
            _context.LoginAccount_Insert(LoginAccountDetail.GroupAccountId, LoginAccountDetail.UserName, LoginAccountDetail.Password,
                LoginAccountDetail.FullName, LoginAccountDetail.Phone, LoginAccountDetail.Email, LoginAccountDetail.Address, LoginAccountDetail.DateTimeCreate,
                LoginAccountDetail.LastUpdate, LoginAccountDetail.LastLogin, LoginAccountDetail.Status, LoginAccountDetail.AgencyId);
        }
        #endregion
        #region Cập nhật mật khẩu tài khoản đăng nhập bằng PROCEDURE
        /// <summary>
        /// Cập nhật mật khẩu tài khoản đăng nhập bằng PROCEDURE
        /// </summary>
        /// <param name="AccountId">Id tài khoản</param>
        /// <param name="Password">Mật khẩu mới</param>
        public void LoginAccount_ChangePassword(int AccountId, string Password)
        {
            string PasswordEncrypt = CMS.Common.StringProcess.Encryption.Md5Encrypt(Password);
            _context.LoginAccount_UpdatePasswordByPK(AccountId, PasswordEncrypt);

        }
        #endregion
        #region Lấy thông tin tài khoản thông qua id tài khoản
        /// <summary>
        /// Lấy thông tin tài khoản thông qua id tài khoản
        /// </summary>
        /// <param name="AccountId">Id tài khoản</param>
        /// <returns></returns>
        public LoginAccount LoginAccount_GetById(int AccountId)
        {
            LoginAccount LoginAccountDetail;

            LoginAccountDetail = _context.LoginAccounts.First(la => la.Id == AccountId);
            return LoginAccountDetail;
        }
        #endregion
        #region Tìm kiếm tài khoản đăng nhập theo nhiều tiêu trí
        /// <summary>
        /// Tìm kiếm tài khoản đăng nhập theo nhiều tiêu trí
        /// </summary>
        /// <param name="UserName">Tên đăng nhập</param>
        /// <param name="BranchId">Id chi nhánh</param>
        /// <param name="BranchCode">Mã chi nhánh</param>
        /// <param name="FullName">Tên nhân viên</param>
        /// <param name="IsUseOTP">Có sử dụng OTP hay không</param>
        /// <param name="Email">Email</param>
        /// <param name="Address">Địa chỉ</param>
        /// <param name="IsSystemAdmin">Là quản trị hệ thống hay không</param>
        /// <param name="IsBranchAdmin">Là quản trị chi nhánh hay không</param>
        /// <param name="LastLoginFrom">Thời gian đăng nhập cuối dùng từ</param>
        /// <param name="LastLoginTo">Thời gian đăng nhập cuối dùng đến</param>
        /// <returns></returns>
        public List<LoginAccount> LoginAccount_Search(string UserName, string FullName, int status)
        {
            #region filter UserName
            Expression<Func<LoginAccount, bool>> filterUserName = la => la.UserName != null;
            if (UserName.Trim() != "")
            {
                filterUserName = la => la.UserName.Contains(UserName);
            }
            #endregion
            #region FilterFullName
            Expression<Func<LoginAccount, bool>> FilterFullName = la => la.FullName != null;
            if (FullName.Trim() != "")
            {
                FilterFullName = la => la.FullName.Contains(FullName);
            }
            #endregion
            #region filter Status
            Expression<Func<LoginAccount, bool>> filterStatus = la => la.Id != 0;
            filterStatus = la => la.Status == status;
            #endregion
            List<LoginAccount> LoginAccountList = new List<LoginAccount>();

            LoginAccountList = _context.LoginAccounts
                .Where(filterUserName)
                .Where(FilterFullName)
                .Where(filterStatus)
                .OrderByDescending(a => a.DateTimeCreate)
                .ThenBy(a => a.FullName)
                .ToList();
            return LoginAccountList;
        }
        #endregion
        #region Lấy danh sách theo status bằng PROCEDURE
        /// <summary>
        /// Lấy danh sách theo status bằng PROCEDURE
        /// </summary>
        /// <param name="status">Trạng thái bản ghi</param>
        public List<LoginAccount_GetByStatus_Result> LoginAccount_GetByStatus(int status)
        {
            return _context.LoginAccount_GetByStatus(status).OrderBy(a => a.Id).ToList();
        }
        #endregion
        #region Lấy danh sách theo đại lý bằng PROCEDURE
        /// <summary>
        /// Lấy danh sách theo status bằng PROCEDURE
        /// </summary>
        /// <param name="id">AccountId</param>
        public List<LoginAccount_GetByGroupAccountId_Result> LoginAccount_GetByGroupAccountId(int id, int status, int agencyId)
        {
            return _context.LoginAccount_GetByGroupAccountId(id, status, agencyId).OrderBy(a => a.Id).ToList();
        }
        #endregion
        #region Chi tiết bản ghi
        /// <summary>
        /// Chi tiết bản ghi
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        public LoginAccount LoginAccount_ViewDetail(int id)
        {
            return _context.LoginAccounts.Find(id);
        }
        #endregion
    }
}
