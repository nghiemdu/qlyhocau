﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Model;

namespace CMS.Dao
{
   public class ConfigDao
    {
        QLyHoCauEntities _context = new QLyHoCauEntities();
        #region Lấy danh sách theo status
        /// <summary>
        /// Lấy danh sách tài status
        /// </summary>
        /// <param name="status">Trạng thái bản ghi</param>
        public List<Config_GetByStatus_Result> Config_GetByStatus(int status)
        {
            return _context.Config_GetByStatus(status).OrderBy(a => a.Id).ToList();
        }
        #endregion
        #region Chi tiết bản ghi
        /// <summary>
        /// Chi tiết bản ghi
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        public Config Configs_ViewDetail(int id)
        {
            return _context.Configs.Find(id);
        }
        #endregion
        #region Cập nhật thông tin
        /// <summary>
        /// Cập nhật thông tin
        /// </summary>
        /// <param name="detail">Thông tin</param>
        public void Configs_Update(Config detail)
        {
            _context.Configs.Attach(detail);
            _context.Entry(detail).Property(la => la.Content).IsModified = true;
            _context.SaveChanges();
        }
        #endregion
    }
}
