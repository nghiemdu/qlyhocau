﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CMS.Model;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;

namespace CMS.Dao
{
    public class GroupAccountDao
    {
        QLyHoCauEntities _context = new QLyHoCauEntities();
        #region Lấy danh sách theo status bằng PROCEDURE
        /// <summary>
        /// Lấy danh sách theo status bằng PROCEDURE
        /// </summary>
        /// <param name="status">Trạng thái bản ghi</param>
        public List<GroupAccount_GetByStatus_Result> GroupAccount_GetByStatus(int status)
        {
            return _context.GroupAccount_GetByStatus(status).OrderBy(a => a.Id).ToList();
        }
        #endregion
        #region Lấy bản ghi theo Id bằng PROCEDURE
        /// <summary>
        /// Lấy bản ghi theo Id bằng PROCEDURE
        /// </summary>
        /// <param name="Id">Mã</param>
        public GroupAccount_GetByPK_Result GroupAccount_GetById(int Id)
        {
            return _context.GroupAccount_GetByPK(Id).OrderBy(a => a.Id).First();
        }
        #endregion
        #region Cập nhật status bằng PROCEDURE
        /// <summary>
        /// Cập nhật status bằng PROCEDURE
        /// </summary>
        /// <param name="status">Trạng thái bản ghi</param>
        public void GroupAccount_UpdateStatus(int id, int status)
        {
            _context.GroupAccount_UpdateStatus(id, status);
        }
        #endregion
        #region Cập nhật thông tin bản ghi
        /// <summary>
        /// Cập nhật thông tin bản ghi
        /// </summary>
        /// <param name="telephoneBookDetail">Thông tin bản ghi</param>
        public void GroupAccount_UpdateInfor(GroupAccount GroupAccountDetail)
        {
            _context.GroupAccounts.Attach(GroupAccountDetail);
            _context.Entry(GroupAccountDetail).Property(la => la.GroupAccountName).IsModified = true;
            _context.Entry(GroupAccountDetail).Property(la => la.Authority).IsModified = true;
            _context.Entry(GroupAccountDetail).Property(la => la.DateTimeLastUpdate).IsModified = true;
            _context.SaveChanges();
        }
        #endregion
        #region Thêm mới bản ghi bằng PROCEDURE
        /// <summary>
        /// Thêm mới bản ghi bằng PROCEDURE
        /// </summary>
        /// <param name="item">Thông tin bản ghi</param>
        public void GroupAccount_Insert(GroupAccount item)
        {
            _context.GroupAccount_Insert(item.GroupAccountName, item.Authority, item.DateTimeCreate,
                item.DateTimeLastUpdate, item.Status);
        }
        #endregion
    }
}
