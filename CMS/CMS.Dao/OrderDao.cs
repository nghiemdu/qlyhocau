﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CMS.Model;

namespace CMS.Dao
{
    public class OrderDao
    {
        QLyHoCauEntities _context = new QLyHoCauEntities();
        #region Thêm mới bằng PROCEDURE
        /// <summary>
        /// Thêm mới bằng PROCEDURE
        /// </summary>
        /// <param name="newItem">Thông tin item</param>
        public int Order_Insert(Order item)
        {
            _context.Order_Insert(item.Name, item.Mobile, item.Address, item.Email, item.DateTimeStart,
                item.DateTimeEnd, item.DateTimeCreate, item.LoginAccountCheckin, item.LoginAccountCheckout, 
                item.LoginAccountCreate, item.Payments, item.Promotion, item.TotalPrice, item.TotalPayment,
                item.Note, item.Value01, item.Value02, item.Value03, item.Value04, item.Value05, item.Status);
            return item.Id;
        }
        #endregion

        #region Lấy danh sách theo status ra by theo page
        /// <summary>
        /// Lấy danh sách tài status
        /// </summary>
        /// <param name="status">Trạng thái bản ghi</param>
        /// <param name="page">Trang số</param>
        /// <param name="pageSize">Số bản ghi trên 1 trang</param>
        public List<Order_GetByPage_Result> Order_GetByPage(int status, int page, int pageSize)
        {
            return _context.Order_GetByPage(status, page, pageSize).ToList();
        }
        #endregion

        #region Chi tiết bản ghi
        /// <summary>
        /// Chi tiết bản ghi
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        public Order Orders_ViewDetail(int id)
        {
            return _context.Orders.Find(id);
        }
        #endregion

        #region Cập nhật trạng thái bản ghi
        /// <summary>
        /// Cập nhật trạng thái bản ghi
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="status">Trạng thái bản ghi</param>
        public int Order_UpdateStatus(int id, int status)
        {
            return _context.Order_UpdateStatus(id, status);
        }
        #endregion

        #region Cập nhật thông tin
        /// <summary>
        /// Cập nhật thông tin
        /// </summary>
        /// <param name="detail">Thông tin</param>
        public void Orders_Update(Order detail)
        {
            _context.Orders.Attach(detail);
            _context.Entry(detail).Property(la => la.Name).IsModified = true;
            _context.Entry(detail).Property(la => la.Mobile).IsModified = true;
            _context.Entry(detail).Property(la => la.Email).IsModified = true;
            _context.Entry(detail).Property(la => la.Payments).IsModified = true;
            _context.Entry(detail).Property(la => la.Address).IsModified = true;
            _context.Entry(detail).Property(la => la.Note).IsModified = true;
            _context.Entry(detail).Property(la => la.DateTimeStart).IsModified = true;
            _context.Entry(detail).Property(la => la.DateTimeEnd).IsModified = true;
            _context.Entry(detail).Property(la => la.LoginAccountCheckout).IsModified = true;
            _context.Entry(detail).Property(la => la.Promotion).IsModified = true;
            _context.Entry(detail).Property(la => la.TotalPayment).IsModified = true;
            _context.Entry(detail).Property(la => la.TotalPrice).IsModified = true;
            _context.Entry(detail).Property(la => la.Value01).IsModified = true;
            _context.Entry(detail).Property(la => la.Value02).IsModified = true;
            _context.Entry(detail).Property(la => la.Value03).IsModified = true;
            _context.Entry(detail).Property(la => la.Value04).IsModified = true;
            _context.Entry(detail).Property(la => la.Value05).IsModified = true;
            _context.SaveChanges();
        }
        #endregion

        #region Cập nhật status
        /// <summary>
        /// Cập nhật status
        /// </summary>
        /// <param name="detail">Thông tin</param>
        public void Orders_UpdateStatus(Order detail)
        {
            _context.Orders.Attach(detail);
            _context.Entry(detail).Property(la => la.Status).IsModified = true;
            _context.Entry(detail).Property(la => la.Value05).IsModified = true;
            _context.SaveChanges();
        }
        #endregion

        #region Cập nhật checkout
        /// <summary>
        /// Cập nhật checkout
        /// </summary>
        /// <param name="detail">Thông tin</param>
        public void Orders_UpdateTotalPrice(Order detail)
        {
            _context.Orders.Attach(detail);
            _context.Entry(detail).Property(la => la.TotalPrice).IsModified = true;
            _context.Entry(detail).Property(la => la.Promotion).IsModified = true;
            _context.Entry(detail).Property(la => la.TotalPayment).IsModified = true;
            _context.Entry(detail).Property(la => la.TotalPrice).IsModified = true;
            _context.Entry(detail).Property(la => la.Note).IsModified = true;
            _context.Entry(detail).Property(la => la.Value05).IsModified = true;
            _context.Entry(detail).Property(la => la.DateTimeEnd).IsModified = true;
            _context.Entry(detail).Property(la => la.LoginAccountCheckout).IsModified = true;
            _context.SaveChanges();
        }
        #endregion

        #region Tìm kiếm
        /// <summary>
        /// Tìm kiếm 
        /// </summary>
        public List<Order> OrderSearch
        (
            string key,
            DateTime dateTimeStart,
            DateTime dateTimeEnd
        )
        {
            #region filterkey
            Expression<Func<Order, bool>> filterkey = la => la.Id != 0;
            if (key != "")
            {
                filterkey = la => la.Value01.Contains(key) || la.Mobile.Contains(key);
            }
            #endregion
            List<Order> list = new List<Order>();
            list = _context.Orders
                .Where(filterkey)
                .Where(la => la.DateTimeCreate >= dateTimeStart && la.DateTimeCreate <= dateTimeEnd)
                .OrderByDescending(a => a.DateTimeCreate)
                .ToList();
            return list;
        }
        #endregion
    }
}
