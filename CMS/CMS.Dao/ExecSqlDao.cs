﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Model;
namespace CMS.Dao
{
    public class ExecSqlDao
    {
        QLyHoCauEntities _context = new QLyHoCauEntities();
        public int ExecSql(string sqlText)
        {
            return _context.Database.ExecuteSqlCommand(sqlText);
        }
    }
}
