﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Model;

namespace CMS.Dao
{
    public class HistoryDao
    {
        QLyHoCauEntities _context = new QLyHoCauEntities();

        public void Insert(int id, string content)
        {
            History newHistory = new History();
            newHistory.Action = "";
            newHistory.LoginAccountId = id;
            newHistory.Content = content;
            newHistory.DateTimeCreate = DateTime.Now;
            try
            {
                _context.History_Insert(newHistory.Action, newHistory.LoginAccountId, newHistory.Content, newHistory.DateTimeCreate);
            }
            catch (Exception)
            {

            }
        }
        public List<History_GetByPage_Result> History_GetByPage(int page, int pageSize)
        {
            return _context.History_GetByPage(page, pageSize).ToList();
        }
    }
}
