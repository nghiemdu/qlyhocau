﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CMS.Model;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace CMS.Dao
{
    public class PriceBoardDao
    {
        QLyHoCauEntities _context = new QLyHoCauEntities();
        #region Lấy danh sách theo status bằng PROCEDURE
        /// <summary>
        /// Lấy danh sách theo status bằng PROCEDURE
        /// </summary>
        /// <param name="status">Trạng thái bản ghi</param>
        public List<PriceBoard_GetByStatus_Result> PriceBoard_GetByStatus(int status)
        {
            return _context.PriceBoard_GetByStatus(status).ToList();
        }
        #endregion

        #region Cập nhật status bằng PROCEDURE
        /// <summary>
        /// Cập nhật status bằng PROCEDURE
        /// </summary>
        /// <param name="status">Trạng thái bản ghi</param>
        public void PriceBoard_UpdateStatus(int id, int status)
        {
            _context.PriceBoard_UpdateStatus(id, status);
        }
        #endregion

        #region Cập nhật thông tin bản ghi
        /// <summary>
        /// Cập nhật thông tin bản ghi
        /// </summary>
        /// <param name="item">Thông tin bản ghi</param>
        public void PriceBoard_UpdateInfor(PriceBoard item)
        {
            _context.PriceBoards.Attach(item);
            _context.Entry(item).Property(la => la.TotalTimeStart).IsModified = true;
            _context.Entry(item).Property(la => la.TotalTimeEnd).IsModified = true;
            _context.Entry(item).Property(la => la.Price).IsModified = true;
            _context.Entry(item).Property(la => la.Note).IsModified = true;
            _context.Entry(item).Property(la => la.Value01).IsModified = true;
            _context.Entry(item).Property(la => la.Value02).IsModified = true;
            _context.Entry(item).Property(la => la.Value03).IsModified = true;
            _context.SaveChanges();
        }
        #endregion

        #region Thêm mới bằng PROCEDURE
        /// <summary>
        /// Thêm mới bằng PROCEDURE
        /// </summary>
        /// <param name="item">Thông tin bản ghi</param>
        public void PriceBoard_Insert(PriceBoard item)
        {
            _context.PriceBoard_Insert(item.TotalTimeStart, item.TotalTimeEnd, item.Price, item.Note, item.Value01, item.Value02, item.Value03, item.Status);
        }
        #endregion

        #region Chi tiết bản ghi
        /// <summary>
        /// Chi tiết bản ghi
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        public PriceBoard PriceBoard_ViewDetail(int id)
        {
            return _context.PriceBoards.Find(id);
        }
        #endregion

        #region Tìm kiếm
        /// <summary>
        /// Tìm kiếm 
        /// </summary>
        public List<PriceBoard> PriceBoardSearch
        (
            int status,
            double totalTime
        )
        {
            #region filterTotalTime
            Expression<Func<PriceBoard, bool>> filterTotalTime = la => la.Id != 0;
            if (totalTime != 0)
            {
                filterTotalTime = la => (la.TotalTimeStart * 60) <= (double)totalTime && (double)totalTime <= (la.TotalTimeEnd * 60);
            }
            #endregion
            List<PriceBoard> list = new List<PriceBoard>();
            list = _context.PriceBoards
                .Where(filterTotalTime)
                .Where(la => la.Status == status)
                //.OrderByDescending(a => a.Value01)
                .ToList();
            return list;
        }
        #endregion
    }
}
