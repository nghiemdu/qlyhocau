﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Dao;
using System.Web.Routing;
using CMS.Model;

namespace CMS.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllersName = "";
            string actionName = "";
            string id = "";
            var routeValues = Request.RequestContext.RouteData.Values;
            if (routeValues.ContainsKey("controller"))
            {
                controllersName = routeValues["controller"].ToString();
                ViewBag.ControllersName = controllersName;
            }
            if (routeValues.ContainsKey("action"))
            {
                actionName = routeValues["action"].ToString();
                ViewBag.ActionName = actionName;
            }
            if (routeValues.ContainsKey("id"))
            {
                id = routeValues["id"].ToString();
            }
            base.OnActionExecuting(filterContext);
        }
        protected void SetAlert(string message, string type)
        {
            TempData["AlertMessage"] = message;
            if (type == "alert-success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "alert-warning")
            {
                TempData["AlertType"] = "alert-warning";
            }
            else if (type == "alert-danger")
            {
                TempData["AlertType"] = "alert-danger";
            }
            else if (type == "label-success")
            {
                TempData["AlertType"] = "label-success";
            }
            else if (type == "label-warning")
            {
                TempData["AlertType"] = "label-warning";
            }
            else if (type == "label-danger")
            {
                TempData["AlertType"] = "label-danger";
            }
            else if (type == "label-info")
            {
                TempData["AlertType"] = "label-info";
            }
        }
    }
}