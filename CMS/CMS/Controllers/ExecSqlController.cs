﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;

namespace CMS.Controllers
{
    public class ExecSqlController : Controller
    {
        //
        // GET: /ExecSql/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /ExecSql/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /ExecSql/Create

        public ActionResult Create(CMS.Models.ExecSqlModel item)
        {
            try
            {
                // TODO: Add insert logic here
                CMS.Dao.ExecSqlDao ExecSqlLogic = new ExecSqlDao();
                if (item.SqlText != null)
                {
                    ExecSqlLogic.ExecSql(item.SqlText);
                    return RedirectToAction("Create");
                }
                return View();
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }

        //
        // POST: /ExecSql/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection, CMS.Models.ExecSqlModel item)
        {
            try
            {
                // TODO: Add insert logic here
                CMS.Dao.ExecSqlDao ExecSqlLogic = new ExecSqlDao();
                if (item.SqlText != null)
                {
                    ExecSqlLogic.ExecSql(item.SqlText);
                }
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }

        //
        // GET: /ExecSql/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /ExecSql/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ExecSql/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ExecSql/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}