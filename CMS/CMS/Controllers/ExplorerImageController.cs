﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Lib;
using CMS.Models;

namespace CMS.Controllers
{
    public class ExplorerImageController : BaseController
    {
        //
        // GET: /ExplorerImage/
        public ActionResult Index(FormCollection formCollection, string path, string id)
        {
            string realPath = Server.MapPath("~/Upload/" + path);
            ViewBag.Path = path;
            // or realPath = "FullPath of the folder on server" 
            if (System.IO.File.Exists(realPath))
            {
                return base.File(realPath, "application/octet-stream");
            }
            else if (System.IO.Directory.Exists(realPath))
            {
                Uri url = Request.Url;
                //Every path needs to end with slash
                if (url.ToString().Last() != '/')
                {
                    Response.Redirect(string.Format("/ExplorerImage/{0}/{1}/", id, path));
                }
                List<DirModel> dirListModel = new List<DirModel>();
                IEnumerable<string> dirList = Directory.EnumerateDirectories(realPath);
                foreach (string dir in dirList)
                {
                    DirectoryInfo d = new DirectoryInfo(dir);
                    DirModel dirModel = new DirModel();
                    dirModel.DirName = Path.GetFileName(dir);
                    dirModel.DirAccessed = d.LastAccessTime;
                    dirListModel.Add(dirModel);
                }
                List<FileModel> fileListModel = new List<FileModel>();
                List<FileModel> fileListModelNew = new List<FileModel>();
                IEnumerable<string> fileList = Directory.EnumerateFiles(realPath);
                foreach (string file in fileList)
                {
                    FileInfo f = new FileInfo(file);
                    FileModel fileModel = new FileModel();
                    if (f.Extension.ToLower() == ".png" || f.Extension.ToLower() == ".jpg" ||
                        f.Extension.ToLower() == ".jpeg" || f.Extension.ToLower() == ".gif" ||
                        f.Extension.ToLower() == ".bmp" || f.Extension.ToLower() == ".doc" ||
                        f.Extension.ToLower() == ".docx" || f.Extension.ToLower() == ".xls" ||
                        f.Extension.ToLower() == ".xlsx" || f.Extension.ToLower() == ".pdf" ||
                        f.Extension.ToLower() == ".rar" || f.Extension.ToLower() == ".zip")
                    {
                        fileModel.FileName = Path.GetFileName(file);
                        fileModel.FileAccessed = f.LastAccessTime;
                        fileModel.FileLocation = (Request.Url.AbsolutePath + Path.GetFileName(file)).ToString().Replace("~", "").Replace("ExplorerImage", "Upload").Replace("explorerimage", "Upload").Replace("/" + id, "");
                        fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : f.Length / 1024 + " KB";
                        fileModel.FileExtension = f.Extension.ToLower();
                        fileListModel.Add(fileModel);
                    }
                }
                fileListModelNew = fileListModel.OrderByDescending(b => b.FileAccessed).ToList();
                ExplorerModel explorerModel = new ExplorerModel(dirListModel, fileListModelNew);
                return View(explorerModel);
            }
            else
            {
                return Content(path + " is not a valid file or directory.");
            }
        }

        //Add folder
        public JsonResult AddFolder(string folderLocation, string name)
        {
            name = CMS.Common.Utility.Utility.ChangeAv(name);
            string urlPage = folderLocation;
            //folderLocation = (folderLocation.Split(','))[0];
            string folderLocationNew = "";
            urlPage = urlPage.Replace("http://", "");
            urlPage = urlPage.Replace("https://", "");
            string[] list = urlPage.Split('/');
            for (int i = 3; i < list.Count() - 1; i++)
            {
                folderLocationNew += "\\" + list[i];
            }

            folderLocationNew += "\\" + name;
            string fullPath = Server.MapPath("/Upload") + folderLocationNew;

            try
            {
                if (Directory.Exists(fullPath) != true)
                {
                    Directory.CreateDirectory(fullPath);
                }
                return Json(folderLocation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(folderLocation + ex, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        [HttpPost]
        public ActionResult UploadImages(HttpPostedFileBase FileUpload, string id, string path)
        {
            if (FileUpload != null && FileUpload.ContentLength > 0)
            {
                string errorMess = "";
                var _fileName = Path.GetFileName(FileUpload.FileName);
                var _ext = Path.GetExtension(FileUpload.FileName);
                #region Kiểm tra phần đuôi mở rộng có phải là file ảnh không
                if (_ext.ToLower() != ".png" && _ext.ToLower() != ".jpg" &&
                    _ext.ToLower() != ".jpeg" && _ext.ToLower() != ".gif" &&
                    _ext.ToLower() != ".bmp" && _ext.ToLower() != ".doc" &&
                    _ext.ToLower() != ".docx" && _ext.ToLower() != ".xls" &&
                    _ext.ToLower() != ".xlsx" && _ext.ToLower() != ".pdf" &&
                    _ext.ToLower() != ".rar" && _ext.ToLower() != ".zip")
                {
                    errorMess += "Hệ thống chỉ chấp nhận file ảnh (*.Jpg, *.Png, *.jpeg). ";
                    //return Json("Upload không thành công! Hệ thống chỉ chấp nhận file ảnh (*.Jpg, *.Png).", JsonRequestBehavior.AllowGet); ;
                }
                #endregion
                int _fileSize = FileUpload.ContentLength;
                if (_fileSize > 2097152)//2Mb
                {
                    errorMess += string.Format("Hệ thống chỉ chấp nhận file có dung lượng dưới 2Mb [File của bạn là {0}] ", _fileSize);
                }

                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return RedirectToAction(string.Format("/{0}/{1}", id, path));
                }
                var _comPath = Path.Combine(Server.MapPath("~/Upload/" + path), _fileName);
                FileUpload.SaveAs(_comPath);
            }

            return Redirect(string.Format("/ExplorerImage/{0}/{1}", id, path));
        }

        //Upload image
        public JsonResult UploadFile(string path)
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var _fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    #region Kiểm tra phần đuôi mở rộng có phải là file ảnh không
                    if (_ext != ".PNG" && _ext != ".JPG" && _ext != ".png" && _ext != ".jpg")
                    {
                        return Json("Upload không thành công! Hệ thống chỉ chấp nhận file ảnh (*.Jpg, *.Png).", JsonRequestBehavior.AllowGet); ;
                    }
                    #endregion

                    #region Kiểm tra xem file name có bị trùng không
                    string realPath = Server.MapPath("~/Upload/" + path);
                    IEnumerable<string> fileList = Directory.EnumerateFiles(realPath);
                    foreach (string file in fileList)
                    {
                        FileInfo f = new FileInfo(file);
                        //FileModel fileModel = new FileModel();

                        if (f.Extension.ToLower() == ".PNG" || f.Extension.ToLower() == ".png" ||
                            f.Extension.ToLower() == ".JPG" || f.Extension.ToLower() == ".jpg")
                        {
                            //fileModel.FileName = Path.GetFileName(file);
                            if (_fileName == Path.GetFileName(file))
                            {
                                return Json("Upload không thành công! Trên hệ thống đã tồn tại file: " + _fileName, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    #endregion
                    var _comPath = string.Format("{0}{1}/{2}", Server.MapPath("/Upload/"), path, _fileName);
                    ViewBag.Msg = _comPath;
                    var pathImg = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(pathImg);
                }
            }
            return Json("Upload thành công!", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteImage(string fileDelete)
        {
            fileDelete = fileDelete.Replace("/Upload/", "");
            fileDelete = fileDelete.Replace("/", "\\");
            string fullPath = Server.MapPath("/Upload/") + fileDelete;
            try
            {
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                return Json("Xoá file thành công!", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Lỗi: " + ex, JsonRequestBehavior.AllowGet);
                throw;
            }

        }
        public JsonResult Deletefolder(string FolderName, string Location)
        {
            Location = Location.Replace("http://", "");
            Location = Location.Replace("https://", "");
            string folderLocationNew = "";
            string[] list = Location.Split('/');
            for (int i = 3; i < list.Count() - 1; i++)
            {
                folderLocationNew += "\\" + list[i];
            }

            folderLocationNew += "\\" + FolderName;
            string fullPath = Server.MapPath("/Upload") + folderLocationNew;

            try
            {
                if (Directory.Exists(fullPath))
                {
                    Directory.Delete(fullPath);
                }
                return Json("Xoá thư mục thành công!", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("Bạn phải xóa hết các file và thư mục bên trong.", JsonRequestBehavior.AllowGet);
                throw;
            }

        }

        // GET: /ExplorerImage/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /ExplorerImage/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ExplorerImage/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ExplorerImage/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /ExplorerImage/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ExplorerImage/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ExplorerImage/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}