﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "1",
                url: "trang-chu",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "2",
                url: "dang-nhap",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "logout",
                url: "logout",
                defaults: new { controller = "Login", action = "Logout", id = UrlParameter.Optional },
                namespaces: new string[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "ManagerLogout",
                url: "Manager/logout",
                defaults: new { controller = "Login", action = "Logout", id = UrlParameter.Optional },
                namespaces: new string[] { "CMS.Areas.Admin.Controllers" }
            );
            routes.MapRoute(
                name: "3",
                url: "Lien-He",
                defaults: new { controller = "Contact", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "4",
                url: "Quyen-Gop",
                defaults: new { controller = "Donate", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "5",
                url: "Thong-bao-License",
                defaults: new { controller = "License", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
               name: "6",
               url: "Trung-tam-cham-soc-CareCenter",
               defaults: new { controller = "CareCenter", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
                name: "7",
                url: "He-Thong-Tochi",
                defaults: new { controller = "Product", action = "ListTochi", id = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
               name: "8",
               url: "dang-ky-ma-the",
               defaults: new { controller = "Contact", action = "DangKyMaThe", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );



            routes.MapRoute(
                name: "51",
                url: "Lien-He/{action}",
                defaults: new { controller = "Contact", action = "{action}", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "52",
                url: "Video-{id}",
                defaults: new { controller = "Video", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
               name: "53",
               url: "Album-Hinh-Anh-{id}",
               defaults: new { controller = "Album", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "54",
               url: "Hinh-Anh-{id}",
               defaults: new { controller = "Album", action = "Details", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "55",
               url: "{tag}-ln{id}",
               defaults: new { controller = "News", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "56",
               url: "{tag}-n{id}",
               defaults: new { controller = "News", action = "Details", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "57",
               url: "{tag}-lp{id}",
               defaults: new { controller = "Product", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "58",
               url: "{tag}-p{id}",
               defaults: new { controller = "Product", action = "Details", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "59",
               url: "Tim-Kiem-{id}",
               defaults: new { controller = "Search", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
        name: "60",
        url: "Thanh-Toan",
        defaults: new { controller = "Cart", action = "Payment", id = UrlParameter.Optional },
        namespaces: new[] { "CMS.Controllers" }
    );
            routes.MapRoute(
            name: "61",
            url: "Them-Gio-Hang-{productId}-{quantity}",
            defaults: new { controller = "Cart", action = "AddItem", id = UrlParameter.Optional },
            namespaces: new[] { "CMS.Controllers" }
        );
            routes.MapRoute(
          name: "62",
          url: "Hoan-Thanh",
          defaults: new { controller = "Cart", action = "Success", id = UrlParameter.Optional },
          namespaces: new[] { "CMS.Controllers" }
      );
            routes.MapRoute(
         name: "63",
         url: "Gio-Hang",
         defaults: new { controller = "Cart", action = "Index", id = UrlParameter.Optional },
         namespaces: new[] { "CMS.Controllers" }
     );
            routes.MapRoute(
               name: "64",
               url: "{tag}-Regulations-{id}",
               defaults: new { controller = "Regulations", action = "Details", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "65",
               url: "{tag}-SearchProduct-{id}",
               defaults: new { controller = "SearchProduct", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "66",
               url: "Ve-tri-thuc-tre",
               defaults: new { controller = "News", action = "About", id = "16" },
               namespaces: new[] { "CMS.Controllers" }
           );







            routes.MapRoute(
                name: "100",
                url: "nhom-quyen",
                defaults: new { controller = "GroupAccount", action = "Index", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "101",
                url: "cau-lenh-sql",
                defaults: new { controller = "ExecSql", action = "Index", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "102",
                url: "sql-them",
                defaults: new { controller = "ExecSql", action = "Create", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "103",
                url: "Chat",
                defaults: new { controller = "Chat", action = "Index", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );



            routes.MapRoute(
                name: "ExplorerVideo",
                url: "ExplorerVideo/{id}/{*path}",
                defaults: new { controller = "ExplorerVideo", action = "Index", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
               name: "ExplorerImageFirt",
               url: "ExplorerImage/UploadFile/{*path}",
               defaults: new { controller = "ExplorerImage", action = "UploadFile", path = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
               name: "ExplorerImage23",
               url: "UploadImagesExplorerImage/{id}/{*path}",
               defaults: new { controller = "ExplorerImage", action = "UploadImages", path = UrlParameter.Optional },
               namespaces: new[] { "CMS.Controllers" }
           );
            routes.MapRoute(
                name: "ExplorerImage",
                url: "ExplorerImage/{id}/{*path}",
                defaults: new { controller = "ExplorerImage", action = "Index", id = UrlParameter.Optional, path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "ExplorerImage_1",
                url: "aExplorerImage/{id}/{*path}",
                defaults: new { controller = "ExplorerImage", action = "Index", id = UrlParameter.Optional, path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "DeleteImage",
                url: "DeleteImage",
                defaults: new { controller = "ExplorerImage", action = "DeleteImage", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "Deletefolder",
                url: "Deletefolder",
                defaults: new { controller = "ExplorerImage", action = "Deletefolder", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "AddFolder ",
                url: "AddFolder",
                defaults: new { controller = "ExplorerImage", action = "AddFolder", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "UploadImage",
                url: "UploadImage",
                defaults: new { controller = "UploadImage", action = "Index", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );


            routes.MapRoute(
                name: "sitemap",
                url: "sitemap.xml",
                defaults: new { controller = "Home", action = "SitemapXml", path = UrlParameter.Optional },
                namespaces: new[] { "CMS.Controllers" }
            );
            routes.MapRoute(
                name: "Manager1",
                url: "Manager",
                defaults: new { controller = "Home", Areas = "Admin", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CMS.Areas.Admin.Controllers" }
            );
            routes.MapRoute(
                name: "Manager2",
                url: "manager",
                defaults: new { controller = "Home", Areas = "Admin", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CMS.Areas.Admin.Controllers" }
            );
            routes.MapRoute(
                name: "DefaultAdmin",
                url: "Admin/{controller}/{action}/{id}",
                defaults: new { controller = "{controller}", action = "{action}", path = UrlParameter.Optional },
                namespaces: new string[] { "CMS.Areas.Admin.Controllers" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "CMS.Controllers" }
            );
        }
    }
}