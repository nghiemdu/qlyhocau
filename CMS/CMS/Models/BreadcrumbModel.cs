﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Models
{
    public class BreadcrumbModel
    {
        public string Title { set; get; }
        public string link { set; get; }
    }
}