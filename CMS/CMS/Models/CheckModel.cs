﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Models
{
    public class CheckModel
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public bool Checked { set; get; }

    }
}
