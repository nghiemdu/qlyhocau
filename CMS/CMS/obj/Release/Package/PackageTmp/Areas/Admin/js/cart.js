﻿let text = "";
function myFunction(item, index) {
    text += "<tr><td>" + item.Name + "</td><td><input type='number' style='width: 100px;border: 0px;border-bottom: 1px solid #CCCCCC;' class='form-control' oninput=\"ChangeNumber(" + item.Id + ",'quantity_" + item.Id + "')\" id='quantity_"
        + item.Id + "' name='quantity_" + item.Id + "' min='1' value='" + item.Number + "'></td><td style='text-align:right;'>" + item.Price + "</td><td style='text-align:right;'>"
        + item.TotalPrice + "</td><td style='text-align: right;'><a class='btn bg-danger btn-sm white-box' onclick='RemoveCart(" + item.Id + ")'><i class='fas fa-times' style='color:#FFFFFF;'></i></a></td></tr>";
}
function ChangeNumber(id, inputid) {
    var number = document.getElementById(inputid).value;
    var listId = $('#hdfListId').val();
    var listNumber = $('#hdfListNumber').val();
    //alert(number+"\n" +listId+"\n"+listNumber);
    var tableHender = "<table id='tableCart' class='table table-condensed table-striped table-bordered table-hover no-margin'><tr><th>Sản phẩm</th><th>Số lượng</th><th>Đơn gia</th><th>Thành tiền</th><th>Xóa</th></tr>";
    var tableFooter = "<tr><td></td><td></td><td>Tổng tiền hàng</td><td style='font-weight:bold;text-align:right;'></td></tr></table>";
    var tableResult = "";
    $.ajax({
        url: '/Admin/ManagerOrder/ChangeNumber',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        data: {
            id: id, number: number, listId: listId, listNumber: listNumber
        },
        success: function (data) {
            if (data.status) {
                data.d.forEach(myFunction);//Foreach lấy ra các hàng dữ liệu
                tableFooter = "<tr><td></td><td></td><td>Tổng tiền hàng</td><td style='font-weight:bold;text-align:right;'>" + data.totalPrice + "</td><td></td></tr>";
                var tableFooter2 = "";
                var tableFooter4 = "";
                if (data.totalPromotionPrice != 0) {
                    tableFooter2 = "<tr><td></td><td></td><td>Ưu đãi</td><td style='font-weight:bold;text-align:right;'>" + data.totalPromotionPrice + "</td><td></td></tr>";
                }
                if (data.totalPromotionOrder != 0) {
                    tableFooter4 = "<tr><td></td><td></td><td>Khuyến mại</td><td style='font-weight:bold;text-align:right;'>" + data.totalPromotionOrder + "</td><td></td></tr>";
                }
                var tableFooter3 = "<tr><td></td><td></td><td>Tổng tiền thanh toán</td><td style='font-weight:bold;text-align:right;'>" + data.totalPayment + "</td><td></td></tr></table>";
                tableResult = tableHender + text + tableFooter + tableFooter2 + tableFooter4 + tableFooter3;
                $("#widget-body-main").html(""); // clear before appending new list
                $("#widget-body-main").html(tableResult);
                document.getElementById("hdfListId").value = data.listId;
                document.getElementById("hdfListNumber").value = data.listNumber;
                text = "";
            }
        }
    });
}

function AddCart() {
    var id = $('#ddlBkavCardType').val();
    var listId = $('#hdfListId').val();
    var listNumber = $('#hdfListNumber').val();
    //alert(id);
    //alert(listNumber);
    var tableHender = "<table id='tableCart' class='table table-condensed table-striped table-bordered table-hover no-margin'><tr><th>Sản phẩm</th><th>Số lượng</th><th>Đơn gia</th><th>Thành tiền</th><th>Xóa</th></tr>";
    var tableFooter = "<tr><td></td><td></td><td>Tổng tiền hàng</td><td style='font-weight:bold;text-align:right;'></td></tr></table>";
    var tableResult = "";
    $.ajax({
        url: '/Admin/ManagerOrder/AddCart',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        data: {
            id: id, listId: listId, listNumber: listNumber
        },
        success: function (data) {
            if (data.status) {
                data.d.forEach(myFunction);//Foreach lấy ra các hàng dữ liệu
                tableFooter = "<tr><td></td><td></td><td>Tổng tiền hàng</td><td style='font-weight:bold;text-align:right;'>" + data.totalPrice + "</td><td></td></tr>";
                var tableFooter2 = "";
                var tableFooter4 = "";
                if (data.totalPromotionPrice != 0) {
                    tableFooter2 = "<tr><td></td><td></td><td>Ưu đãi</td><td style='font-weight:bold;text-align:right;'>" + data.totalPromotionPrice + "</td><td></td></tr>";
                }
                if (data.totalPromotionOrder != 0) {
                    tableFooter4 = "<tr><td></td><td></td><td>Khuyến mại</td><td style='font-weight:bold;text-align:right;'>" + data.totalPromotionOrder + "</td><td></td></tr>";
                }
                var tableFooter3 = "<tr><td></td><td></td><td>Tổng tiền thanh toán</td><td style='font-weight:bold;text-align:right;'>" + data.totalPayment + "</td><td></td></tr></table>";
                tableResult = tableHender + text + tableFooter + tableFooter2 + tableFooter4 + tableFooter3;
                $("#widget-body-main").html(""); // clear before appending new list
                $("#widget-body-main").html(tableResult);
                document.getElementById("hdfListId").value = data.listId;
                document.getElementById("hdfListNumber").value = data.listNumber;
                text = "";
            }
        }
    });
}

function SaveCart() {
    var listId = $('#hdfListId').val();
    var listNumber = $('#hdfListNumber').val();
    var email = $('#Email').val();
    var mobile = $('#Mobile').val();
    var note= $('#Note').val();
    //alert(number+"\n" +listId+"\n"+listNumber);
    var tableResult = "";
    $.ajax({
        url: '/Admin/ManagerOrder/SaveCart',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        data: {
            email: email, mobile: mobile, listId: listId, listNumber: listNumber, note: note
        },
        success: function (data) {
            if (data.status) {
                window.location.href = data.d;
            }
        }
    });
}

function RemoveCart(id) {
    var listId = $('#hdfListId').val();
    var listNumber = $('#hdfListNumber').val();
    //alert(text);
    //alert(listNumber);
    var tableHender = "<table id='tableCart' class='table table-condensed table-striped table-bordered table-hover no-margin'><tr><th>Sản phẩm</th><th>Số lượng</th><th>Đơn gia</th><th>Thành tiền</th><th>Xóa</th></tr>";
    var tableFooter = "<tr><td></td><td></td><td>Tổng tiền hàng</td><td style='font-weight:bold;text-align:right;'></td></tr></table>";
    var tableResult = "";
    $.ajax({
        url: '/Admin/ManagerOrder/RemoveCart',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        data: {
            id: id, listId: listId, listNumber: listNumber
        },
        success: function (data) {
            if (data.status) {
                data.d.forEach(myFunction);//Foreach lấy ra các hàng dữ liệu
                tableFooter = "<tr><td></td><td></td><td>Tổng tiền hàng</td><td style='font-weight:bold;text-align:right;'>" + data.totalPrice + "</td><td></td></tr>";
                var tableFooter2 = "";
                var tableFooter4 = "";
                if (data.totalPromotionPrice != 0) {
                    tableFooter2 = "<tr><td></td><td></td><td>Ưu đãi</td><td style='font-weight:bold;text-align:right;'>" + data.totalPromotionPrice + "</td><td></td></tr>";
                }
                if (data.totalPromotionOrder != 0) {
                    tableFooter4 = "<tr><td></td><td></td><td>Khuyến mại</td><td style='font-weight:bold;text-align:right;'>" + data.totalPromotionOrder + "</td><td></td></tr>";
                }
                var tableFooter3 = "<tr><td></td><td></td><td>Tổng tiền thanh toán</td><td style='font-weight:bold;text-align:right;'>" + data.totalPayment + "</td><td></td></tr></table>";
                tableResult = tableHender + text + tableFooter + tableFooter2 + tableFooter4 + tableFooter3;
                $("#widget-body-main").html(""); // clear before appending new list
                $("#widget-body-main").html(tableResult);
                document.getElementById("hdfListId").value = data.listId;
                document.getElementById("hdfListNumber").value = data.listNumber;
                text = "";
            }
        }
    });
}

function CheckTime() {
    var txtHoursStart = $('#HoursStart').val();
    var txtMinuteStart = $('#MinuteStart').val();
    var txtHoursEnd = $('#HoursEnd').val();
    var txtMinuteEnd = $('#MinuteEnd').val();

    var totalTime = ((txtHoursEnd * 60) + parseInt(txtMinuteEnd)) - (txtHoursStart * 60 + parseInt(txtMinuteStart));
    //alert((txtHoursEnd * 60) + parseInt(txtMinuteEnd));
    $.ajax({
        url: '/Admin/ManagerOrder/CheckTime',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        data: {
            totalminute: totalTime
        },
        success: function (data) {
            if (data.status) {
                $("#pTime").html(""); // clear before appending new list
                $("#pTime").html(Math.floor(totalTime / 60) + " giờ " + (totalTime % 60) + " phút <span class='badge bg-info'>" + data.price + "đ</span>");
            }
        }
    });
}