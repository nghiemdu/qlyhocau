/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';

    config.syntaxhighlight_lang = 'csharp';
    config.syntaxhighlight_hideControls = true;
    config.language = 'vi';
    config.filebrowserBrowseUrl = '/Areas/Admin/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/Areas/Admin/ckfinder.html?Type=Images';
    config.filebrowserFlashBrowseUrl = '/Areas/Admin/ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = '/Areas/Admin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '/Upload/Images';
    config.filebrowserFlashUploadUrl = '/Areas/Admin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash';
    config.height = '800px';
    config.extraPlugins = 'lineheight,language';
    CKFinder.setupCKEditor(null, '/Areas/Admin/ckfinder/');

    config.toolbar = [
        {
            name: 'clipboard', items: ['Source', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Undo', 'Redo', 'Link', 'Unlink', 'Anchor',
            '-', 'Image', 'Table', 'HorizontalRule', 'SpecialChar', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat',
            'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'TextColor', 'BGColor', '-', 'Maximize']
        },
        '/',
        { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
        '/',
{ name: 'abc', items: ['lineheight'] }
    ];
};
