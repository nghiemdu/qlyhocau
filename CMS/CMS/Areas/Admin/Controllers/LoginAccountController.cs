﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;
using CMS.Common.StringProcess;

namespace CMS.Areas.Admin.Controllers
{
    public class LoginAccountController : BaseController
    {
        //
        // GET: /Admin/LoginAccount/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Admin/LoginAccount/Details/5

        public ActionResult Details()
        {
            LoginAccount itemLoginAccount = new LoginAccount();
            itemLoginAccount = (LoginAccount)ViewBag.ItemLoginAccount;
            try
            {
                LoginAccountDao logic = new LoginAccountDao();
                LoginAccount item = new LoginAccount();
                item = logic.LoginAccount_ViewDetail(Convert.ToInt32(itemLoginAccount.Id));
                ViewBag.PanelTitle = ViewBag.Title = "Thông tin tài khoản";
                ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text", item.GroupAccountId);
                return View(item);
            }
            catch (Exception)
            {
                return Redirect("/Admin/LoginAccount/Details/");
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Details(LoginAccount item)
        {
            LoginAccount itemLoginAccount = new LoginAccount();
            itemLoginAccount = (LoginAccount)ViewBag.ItemLoginAccount;
            
            ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin tài khoản";
            ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text", item.GroupAccountId);
            try
            {
                // TODO: Add insert logic here
                string errorMess = "";

                if (item.FullName == "" || item.FullName == null)
                {
                    errorMess += "Tên tài khoản không được để trống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                LoginAccountDao logic = new LoginAccountDao();
                item.Id = itemLoginAccount.Id;
                item.LastUpdate = DateTime.Now;
                logic.LoginAccount_UpdateDetail(item);

                #region Insert history
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật thông tin tài khoản. ID: {0}, UserName: {1}", item.Id, item.UserName));
                #endregion
                SetAlert("Cập nhật thành công!", "alert-success");
                return Redirect("/Admin/LoginAccount/Details");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/LoginAccount/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/LoginAccount/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/LoginAccount/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Admin/LoginAccount/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ChangePass()
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thay đổi mật khẩu";
            return View();
        }

        //
        // POST: /Admin/LoginAccount/Edit/5

        [HttpPost]
        public ActionResult ChangePass(FormCollection collection, CMS.Areas.Admin.Models.PassModel item)
        {
            LoginAccount itemLoginAccount = new LoginAccount();
            itemLoginAccount = (LoginAccount)ViewBag.ItemLoginAccount;
            ViewBag.PanelTitle = ViewBag.Title = "Thay đổi mật khẩu";
            try
            {
                LoginAccountDao logic = new LoginAccountDao();
                LoginAccount itemLoginLogic = new LoginAccount();
                itemLoginLogic = logic.LoginAccount_ViewDetail(itemLoginAccount.Id);
                // TODO: Add insert logic here
                string errorMess = "";
                if (item.oldPass == "" || item.oldPass == null)
                {
                    errorMess += "Mật khẩu cũ không được để trống! ";
                }
                else if (item.pass == "" || item.pass == null)
                {
                    errorMess += "Mật khẩu mới không được để trống! ";
                }
                else if (item.confirmPass == "" || item.confirmPass == null)
                {
                    errorMess += "Nhập lại mật khẩu không được để trống! ";
                }
                else
                {
                    if (itemLoginLogic.Password != Encryption.Md5Encrypt(item.oldPass))
                    {
                        errorMess += "Mật khẩu cũ không chính xác! ";
                    }
                    if (item.pass != item.confirmPass)
                    {
                        errorMess += "Mật khẩu và nhập lại mật khẩu phải trùng nhau! ";
                    }
                    if (item.pass.Length < 8)
                    {
                        errorMess += "Mật khẩu phải từ 8 ký tự trở lên! ";
                    }
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                logic.LoginAccount_ChangePassword(itemLoginAccount.Id, item.pass);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật mật khẩu. Username: {0}", itemLoginLogic.UserName));
                #endregion
                SetAlert("Cập nhật thành công!", "alert-success");
                return Redirect("/Admin/LoginAccount/Details/");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/LoginAccount/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Admin/LoginAccount/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public List<SelectListItem> BindListGroupAccount()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Nhân viên", Value = "2" });
            listItems.Add(new SelectListItem { Text = "Quản lý", Value = "3" });
            return listItems;
        }
    }
}
