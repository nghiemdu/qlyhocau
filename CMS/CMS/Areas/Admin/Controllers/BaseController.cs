﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using System.Web.Routing;
using CMS.Dao;

namespace CMS.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn] == null || Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value == null || Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value == "")
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "Index", Area = "Admin" }));
            }
            else
            {
                string loginAccountId = CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value);
                LoginAccountDao LoginDaoLogic = new LoginAccountDao();
                LoginAccount item = new LoginAccount();
                item = LoginDaoLogic.LoginAccount_GetById(Convert.ToInt32(loginAccountId));
                ViewBag.ItemLoginAccount = item;
                TimeSpan checkTimeonline = new TimeSpan();
                checkTimeonline = DateTime.Now - Convert.ToDateTime(item.LastLogin);
                if (checkTimeonline.TotalMinutes > 30)//Nếu lớn hơn 30 phút thì cho logout
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "Index", Area = "Admin" }));
                }
                else
                {
                    #region Cứ mỗi hành đồng lại update thời gian hoạt động
                    LoginDaoLogic.LoginAccount_UpdateLastLogin(item.Id, DateTime.Now);
                    CMS.Areas.Admin.Class.WriteLog.UpdateLog("LOG", "Đăng nhập", "Logic", String.Format("Tài khoản {0} (ID: {1}) đăng nhập thành công.", item.FullName, item.Id));
                    #endregion
                    string controllersName = "";
                    string actionName = "";
                    var routeValues = Request.RequestContext.RouteData.Values;
                    if (routeValues.ContainsKey("controller"))
                    {
                        controllersName = routeValues["controller"].ToString();
                    }
                    if (routeValues.ContainsKey("action"))
                    {
                        actionName = routeValues["action"].ToString();
                    }
                    if (item.Password == CMS.Common.StringProcess.Encryption.Md5Encrypt("12345678") && actionName != "ChangePass")
                    {
                        switch (item.GroupAccountId)
                        {
                            case 1:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "ManagerLoginAccount", action = "ChangePass", Area = "Admin"}));
                                break;
                            case 2:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "LoginAccount", action = "ChangePass", Area = "Admin"}));
                                break;
                            case 3:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "ManagerLoginAccount", action = "ChangePass", Area = "Admin"}));
                                break;
                            default:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "Index", Area = "Admin" }));
                                break;
                        }
                        
                    }
                    ViewBag.ControllersName = controllersName;
                    #region Check Permission
                    GroupAccountDao GroupAccountLogic = new GroupAccountDao();
                    CMS.Model.GroupAccount_GetByPK_Result itemGroupAccount = new CMS.Model.GroupAccount_GetByPK_Result();
                    itemGroupAccount = GroupAccountLogic.GroupAccount_GetById(item.GroupAccountId.Value);
                    string[] listAuthority = null;
                    bool flag = false;
                    string[] listController = null;
                    if (itemGroupAccount.Authority != null)
                    {
                        string sPermissionController = "";
                        Dictionary<string, string> dPermissionController= new Dictionary<string, string>();
                        dPermissionController = CMS.Common.Permission.Permission.ListPermissionController;
                        listAuthority = itemGroupAccount.Authority.Split(',');
                        ViewBag.ListAuthority = listAuthority;
                        foreach (var itemAuthority in listAuthority)
                        {
                            if (dPermissionController.ContainsKey(itemAuthority))
                            {
                                if (sPermissionController == "")
                                {
                                    sPermissionController = dPermissionController[itemAuthority];
                                }
                                else
                                {
                                    sPermissionController += "," + dPermissionController[itemAuthority];
                                }
                            }
                        }
                        listController= sPermissionController.Split(',');
                    }
                    foreach (var itemController in listController)
                    {
                        if(itemController== controllersName)
                        {
                            flag = true;break;
                        }
                    }
                    if (flag == false)
                    {
                        switch (item.GroupAccountId)
                        {
                            case 1:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "HomeManager", action = "Index", Area = "Admin" }));
                                break;
                            case 2:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Index", Area = "Admin" }));
                                break;
                            case 3:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "HomeManager", action = "Index", Area = "Admin" }));
                                break;
                            default:
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "Index", Area = "Admin" }));
                                break;
                        }
                    }
                    #endregion
                }
            }
            #region Get config
            ConfigDao ConfigLogic = new ConfigDao();
            if (ConfigLogic.Configs_ViewDetail(8).Content != null)
            {
                ViewBag.Logo = ConfigLogic.Configs_ViewDetail(8).Content;//Lấy logo
            }
            else { ViewBag.Logo = ""; }
            if (ConfigLogic.Configs_ViewDetail(6).Content != null)
            {
                ViewBag.NumberMobile = ConfigLogic.Configs_ViewDetail(6).Content;//Lấy số điện thoại
            }
            else { ViewBag.NumberMobile = ""; }
            if (ConfigLogic.Configs_ViewDetail(5).Content != null)
            {
                ViewBag.Email = ConfigLogic.Configs_ViewDetail(5).Content.Trim();//Lấy email
            }
            else { ViewBag.Email = ""; }
            #endregion
            base.OnActionExecuting(filterContext);
        }
        protected void SetAlert(string message, string type)
        {
            TempData["AlertMessage"] = message;
            if (type == "alert-success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "alert-warning")
            {
                TempData["AlertType"] = "alert-warning";
            }
            else if (type == "alert-danger")
            {
                TempData["AlertType"] = "alert-danger";
            }
        }
    }
}