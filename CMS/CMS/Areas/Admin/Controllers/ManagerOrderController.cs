﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;
using CMS.Common.StringProcess;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace CMS.Areas.Admin.Controllers
{
    public class ManagerOrderController : BaseController
    {
        //
        // GET: /Admin/ManagerOrder/

        public ActionResult Index(string id)
        {
            string page = "1";
            const int pageSize = 50;
            ViewBag.PageSize = pageSize;
            page = id;
            ViewBag.Page = page;
            OrderDao logic = new OrderDao();
            List<Order_GetByPage_Result> model = new List<Order_GetByPage_Result>();
            model = logic.Order_GetByPage(0, Convert.ToInt32(page), pageSize);
            ViewBag.PanelTitle = ViewBag.Title = "Danh sách đơn hàng";
            return View("Index", model);
        }

        public ActionResult Search(string id)
        {
            string page = "1";
            const int pageSize = 50;
            string key = "";//Không tìm kiếm theo key
            DateTime dStart = new DateTime(1900, 01, 01, 01, 01, 01);
            DateTime dEnd = new DateTime(2100, 12, 31, 23, 59, 59);

            string[] list = id.Split('-');
            page = list[3];
            if (list[0].ToLower() != "null") { key = list[0]; }
            if (list[1].ToLower() != "null")
            {
                string[] listdStart = list[1].Split('_');
                dStart = new DateTime(Convert.ToInt32(listdStart[2]), Convert.ToInt32(listdStart[1]), Convert.ToInt32(listdStart[0]), 01, 01, 01);
            }
            if (list[2].ToLower() != "null")
            {
                string[] listdEnd = list[2].Split('_');
                dEnd = new DateTime(Convert.ToInt32(listdEnd[2]), Convert.ToInt32(listdEnd[1]), Convert.ToInt32(listdEnd[0]), 23, 59, 59);
            }

            ViewBag.Key = list[0];
            ViewBag.dStart = list[1];
            ViewBag.dEnd = list[2];
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;

            int orderSuccess = 0;
            double priceSuccess = 0;

            OrderDao logic = new OrderDao();
            List<Order> model = new List<Order>();
            List<Order> results = new List<Order>();
            model = logic.OrderSearch(key, dStart, dEnd);
            int rowStart = 0;
            int rowEnd = 0;
            if (model != null && model.Count > 0)
            {
                rowStart = (Convert.ToInt32(page) - 1) * Convert.ToInt32(pageSize);
                rowEnd = (rowStart + Convert.ToInt32(pageSize)) > model.Count ? model.Count : rowStart + Convert.ToInt32(pageSize);
                for (int i = rowStart; i < rowEnd; i++)
                {
                    #region Add item
                    Order item = new Order();
                    item = model[i];
                    results.Add(item);
                    #endregion
                }
                foreach(var item in model)
                {
                    if (item.Status == CMS.Common.Keywords.DataStatus.Success)
                    {
                        orderSuccess++;
                        priceSuccess = priceSuccess + item.TotalPayment.Value;
                    }
                }
            }
            ViewBag.TotalCount = String.Format("{0:n0}", model.Count);
            ViewBag.OrderSuccess = String.Format("{0:n0}", orderSuccess);
            ViewBag.PriceSuccess = String.Format("{0:n0}", priceSuccess);
            ViewBag.PanelTitle = ViewBag.Title = String.Format("Tìm kiếm nâng cao: ({0} đơn hàng)", String.Format("{0:n0}", model.Count));
            ViewBag.ControllersName = "ManagerOrderSearch";
            return View("Search", model);
        }

        [HttpGet]
        public FileContentResult ExportToExcel(string id)
        {
            LoginAccountDao LoginAccountLogic = new LoginAccountDao();
            string key = "";//Không tìm kiếm theo key
            DateTime dStart = new DateTime(1900, 01, 01, 01, 01, 01);
            DateTime dEnd = new DateTime(2100, 12, 31, 23, 59, 59);

            string[] list = id.Split('-');
            if (list[0].ToLower() != "null") { key = list[0]; }
            if (list[1].ToLower() != "null")
            {
                string[] listdStart = list[1].Split('_');
                dStart = new DateTime(Convert.ToInt32(listdStart[2]), Convert.ToInt32(listdStart[1]), Convert.ToInt32(listdStart[0]), 01, 01, 01);
            }
            if (list[2].ToLower() != "null")
            {
                string[] listdEnd = list[2].Split('_');
                dEnd = new DateTime(Convert.ToInt32(listdEnd[2]), Convert.ToInt32(listdEnd[1]), Convert.ToInt32(listdEnd[0]), 23, 59, 59);
            }

            ViewBag.Key = list[0];
            ViewBag.dStart = list[1];
            ViewBag.dEnd = list[2];

            OrderDao logic = new OrderDao();
            List<Order> model = new List<Order>();
            model = logic.OrderSearch(key, dStart, dEnd);

            #region Add table
            DataTable dt = new DataTable();
            dt.Columns.Add("DateTimeCreate", typeof(string));
            dt.Columns.Add("TotalTime", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Mobile", typeof(string));
            dt.Columns.Add("TotalPrice", typeof(string));
            dt.Columns.Add("Note", typeof(string));
            dt.Columns.Add("LoginAccountCheckin", typeof(string));
            dt.Columns.Add("LoginAccountCheckout", typeof(string));

            if (model != null && model.Count > 0)
            {
                foreach (var item in model)
                {
                    string DateTimeCreate = "";
                    string TotalTime = "-----";
                    string Name = "";
                    string Mobile = "";
                    string TotalPrice = "";
                    string Note = "";
                    string LoginAccountCheckin = "";
                    string LoginAccountCheckout = "";

                    string DateTimeDay = "";
                    if (item.DateTimeCreate != null)
                    {
                        DateTimeDay = item.DateTimeCreate.Value.ToString("dd/MM/yyyy");
                    }
                    string DateTimeStart = "-----";
                    if (item.DateTimeStart != null)
                    {
                        DateTimeStart = item.DateTimeStart.Value.ToString("HH:mm");
                    }
                    string DateTimeEnd = "-----";
                    if (item.DateTimeEnd != null)
                    {
                        DateTimeEnd = item.DateTimeEnd.Value.ToString("HH:mm");
                    }
                    DateTimeCreate = String.Format("Từ {0} đến {1} ngày {2}", DateTimeStart, DateTimeEnd, DateTimeDay);

                    if (item.DateTimeEnd != null && item.DateTimeStart != null)
                    {
                        int totalminute = (item.DateTimeEnd.Value.Hour * 60 + item.DateTimeEnd.Value.Minute) - (item.DateTimeStart.Value.Hour * 60 + item.DateTimeStart.Value.Minute);
                        TotalTime = String.Format("{0} giờ {1} phút", totalminute / 60, totalminute % 60);
                    }
                    Name = item.Name;
                    Mobile = item.Mobile;
                    Note = item.Note;
                    TotalPrice = String.Format("{0:n0}", item.TotalPayment.Value);
                    if (item.LoginAccountCheckin.Value != 0 && item.LoginAccountCheckin != null)
                    {
                        LoginAccountCheckin = (LoginAccountLogic.LoginAccount_ViewDetail(item.LoginAccountCheckin.Value)).FullName;
                    }
                    if (item.LoginAccountCheckout.Value != 0 && item.LoginAccountCheckout != null)
                    {
                        LoginAccountCheckout = (LoginAccountLogic.LoginAccount_ViewDetail(item.LoginAccountCheckout.Value)).FullName;
                    }
                    dt.Rows.Add(DateTimeCreate, TotalTime, Name, Mobile, TotalPrice, Note, LoginAccountCheckin, LoginAccountCheckout);
                }
            }
            else
            {
                dt.Rows.Add("", "", "", "", "", "", "","");
            }
            #endregion

            string fileName = String.Format("Report_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"));
            string[] columns = { "DateTimeCreate","TotalTime", "Name", "Mobile", "TotalPrice", "Note", "LoginAccountCheckin", "LoginAccountCheckout" };
            string columnsName = "Ngày|Tổng thời gian|Khách hàng|Số điện thoại|Tổng tiền|Ghi chú|Nhân viên check in|Nhân viên check out";
            byte[] filecontent = CMS.Common.Utility.ExcelExportHelper.ExportExcel(dt, "Báo cáo bán hàng", true, columnsName, columns);
            return File(filecontent, CMS.Common.Utility.ExcelExportHelper.ExcelContentType, fileName);
        }

        public JsonResult CheckTime(string id, string hoursStart, string minuteStart, string hoursEnd, string minuteEnd)
        {
            try
            {
                #region Tính tổng thời gian
                double totalminute = 0;
                OrderDao OrderLogic = new OrderDao();
                Order itemOrder = new Order();
                itemOrder = OrderLogic.Orders_ViewDetail(Convert.ToInt32(id));
                DateTime DateTimeStart = new DateTime(itemOrder.DateTimeStart.Value.Year, itemOrder.DateTimeStart.Value.Month, itemOrder.DateTimeStart.Value.Day, Convert.ToInt32(hoursStart), Convert.ToInt32(minuteStart), 0);
                DateTime DateTimeEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(hoursEnd), Convert.ToInt32(minuteEnd), 0);
                TimeSpan ts = new TimeSpan();
                ts = DateTimeEnd.Subtract(DateTimeStart);
                totalminute = Math.Round(ts.TotalMinutes, 0);
                #endregion

                PriceBoardDao logic = new PriceBoardDao();
                List<PriceBoard> list = new List<PriceBoard>();
                list = logic.PriceBoardSearch(CMS.Common.Keywords.DataStatus.Display, totalminute);
                double price = 0;
                if(list!=null&& list.Count > 0)
                {
                    price = list[0].Price.Value;
                }
                return Json(new
                {
                    status = true,
                    totalTime = String.Format("{0} giờ {1} phút", (int)totalminute / 60, (int)totalminute % 60),
                    price = String.Format("{0:n0}", price)
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new
                {
                    status = true,
                    price = 0
                }, JsonRequestBehavior.AllowGet);
            }
            
        }

        //
        // GET: /Admin/ManagerOrder/Details/5

        public ActionResult Details(string id)
        {
            id = Encryption.Decode(id);
            try
            {
                OrderDao logic = new OrderDao();
                Order item = new Order();
                item = logic.Orders_ViewDetail(Convert.ToInt32(id));
                ViewBag.PanelTitle = ViewBag.Title = "Thông tin đơn hàng";
                return View(item);
            }
            catch (Exception)
            {
                return Redirect("/Admin/ManagerOrder/Index/1");
            }
        }

        //
        // GET: /Admin/ManagerOrder/Create

        public ActionResult Create()
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thêm mới đơn hàng";
            return View();
        }

        //
        // POST: /Admin/ManagerOrder/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Order item, string HoursStart, string MinuteStart)
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thêm mới đơn hàng";
            try
            {
                // TODO: Add insert logic here
                string errorMess = "";

                if (item.Name == null|| item.Name == "")
                {
                    errorMess += "Tên khách hàng không được để trống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                LoginAccountDao LoginAccountLogic = new LoginAccountDao();
                LoginAccount itemLoginAccount = new LoginAccount();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                itemLoginAccount = LoginAccountLogic.LoginAccount_ViewDetail(loginId);

                OrderDao logic = new OrderDao();
                item.DateTimeStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(HoursStart), Convert.ToInt32(MinuteStart), 0);
                item.Status = 1;
                item.DateTimeCreate = DateTime.Now;
                item.LoginAccountCheckin = item.LoginAccountCreate = loginId;
                item.LoginAccountCheckout = 0;
                item.Promotion = item.TotalPayment = item.TotalPrice = 0;
                item.Value01 = CMS.Common.Utility.Utility.ChangeAvSearch(item.Name);
                item.Value05 += String.Format("<p>{1}: Tài khoản {0} tạo mới đơn hàng</p>", itemLoginAccount.FullName, item.DateTimeCreate);
                logic.Order_Insert(item);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Thêm mới đơn hàng. Khách hàng {0}", item.Name));
                #endregion
                SetAlert("Thêm mới thành công!", "alert-success");
                return Redirect("/Admin/ManagerOrder/Index/1");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/ManagerOrder/Edit/5

        public ActionResult Edit(string id)
        {
            id = Encryption.Decode(id);
            try
            {
                OrderDao logic = new OrderDao();
                Order item = new Order();
                item = logic.Orders_ViewDetail(Convert.ToInt32(id));
                ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin đơn hàng";
                return View(item);
            }
            catch (Exception)
            {
                return Redirect("/Admin/ManagerOrder/Index/1");
            }
        }

        //
        // POST: /Admin/ManagerOrder/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(string id, Order item, string HoursStart, string MinuteStart, string HoursEnd, string MinuteEnd)
        {
            ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin đơn hàng";
            try
            {
                // TODO: Add insert logic here
                string errorMess = "";

                if (item.Name == null || item.Name == "")
                {
                    errorMess += "Tên khách hàng không được để trống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                LoginAccountDao LoginAccountLogic = new LoginAccountDao();
                LoginAccount itemLoginAccount = new LoginAccount();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                itemLoginAccount = LoginAccountLogic.LoginAccount_ViewDetail(loginId);

                OrderDao logic = new OrderDao();
                OrderDao logicOld = new OrderDao();
                Order itemOld = new Order();
                itemOld = logicOld.Orders_ViewDetail(Convert.ToInt32(Encryption.Decode(id)));

                item.DateTimeStart = new DateTime(itemOld.DateTimeStart.Value.Year, itemOld.DateTimeStart.Value.Month, itemOld.DateTimeStart.Value.Day, Convert.ToInt32(HoursStart), Convert.ToInt32(MinuteStart), 0);
                item.DateTimeEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(HoursEnd), Convert.ToInt32(MinuteEnd), 0);
                item.Status = 1;
                item.LoginAccountCheckout = loginId;
                item.TotalPrice = item.Promotion + item.TotalPayment;
                item.Value01 = CMS.Common.Utility.Utility.ChangeAvSearch(item.Name);
                item.Value05 += String.Format("<p>{1}: Tài khoản {0} cập nhật thông tin đơn hàng</p>", itemLoginAccount.FullName, item.DateTimeCreate);
                item.Id = Convert.ToInt32(Encryption.Decode(id));
                logic.Orders_Update(item);
                #region Insert history`
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật thông tin đơn hàng. Khách hàng {0}", item.Name));
                #endregion
                SetAlert("Cập nhật thành công!", "alert-success");
                return Redirect("/Admin/ManagerOrder/Edit/"+ id);
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        public ActionResult PaymentOrder(string id)
        {
            try
            {
                LoginAccountDao LoginAccountLogic = new LoginAccountDao();
                LoginAccount itemLoginAccount = new LoginAccount();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                itemLoginAccount = LoginAccountLogic.LoginAccount_ViewDetail(loginId);

                id = CMS.Common.StringProcess.Encryption.Decode(id);
                OrderDao logic = new OrderDao();
                Order item = new Order();
                item = logic.Orders_ViewDetail(Convert.ToInt32(id));
                item.Status = CMS.Common.Keywords.DataStatus.Success;
                item.Value05 += String.Format("<p>{1}: Tài khoản {0} thanh toán đơn hàng</p>", itemLoginAccount.FullName, DateTime.Now);
                logic.Orders_UpdateStatus(item);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Thanh toán đơn hàng. Khách hàng {0}", item.Name));
                #endregion
                SetAlert("Thanh toán thành công!", "alert-success");
                return Redirect("/Admin/ManagerOrder/Details/" + Encryption.Encode(id));
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/ManagerOrder/Delete/5

        public ActionResult Delete(string id)
        {
            try
            {
                LoginAccountDao LoginAccountLogic = new LoginAccountDao();
                LoginAccount itemLoginAccount = new LoginAccount();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                itemLoginAccount = LoginAccountLogic.LoginAccount_ViewDetail(loginId);

                string[] list = id.Split('-');
                string pageStyle = list[0];
                id = list[1];
                id = CMS.Common.StringProcess.Encryption.Decode(id);
                OrderDao logic = new OrderDao();
                Order item = new Order();
                item = logic.Orders_ViewDetail(Convert.ToInt32(id));
                item.Status = CMS.Common.Keywords.DataStatus.Deleted;
                item.Value05 += String.Format("<p>{1}: Tài khoản {0} huỷ đơn hàng</p>", itemLoginAccount.FullName, DateTime.Now);
                logic.Orders_UpdateStatus(item);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Chuyển trạng thái bản ghi sang đã hủy. ID: {0}", id.ToString()));
                #endregion
                SetAlert("Chuyển trạng thái thành công!", "alert-success");
                if (pageStyle == "List")
                {
                    return Redirect("/Admin/ManagerOrder/Index/1");
                }
                else
                {
                    return Redirect("/Admin/ManagerOrder/Details/" + Encryption.Encode(id));
                }
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // POST: /Admin/ManagerOrder/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
