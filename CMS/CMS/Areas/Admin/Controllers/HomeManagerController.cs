﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;
using CMS.Common.StringProcess;

namespace CMS.Areas.Admin.Controllers
{
    public class HomeManagerController : BaseController
    {
        //
        // GET: /Admin/HomeManager/

        public ActionResult Index()
        {
            LoginAccount itemLoginAccount = new LoginAccount();
            itemLoginAccount = (LoginAccount)ViewBag.ItemLoginAccount;
            OrderDao logic = new OrderDao();

            #region Tính tiền cho ngày hiện tại
            DateTime dStartToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 01, 01, 01);
            DateTime dEndToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            List<Order> listToday = new List<Order>();
            listToday = logic.OrderSearch("", dStartToday, dEndToday);
            int todayTotalOrder = 0;
            int todayTotalOrderDisplay = 0;
            int todayTotalOrderSuccess = 0;
            double todayTotalPrice = 0;
            if (listToday != null && listToday.Count > 0)
            {
                todayTotalOrder = listToday.Count;
                foreach (var item in listToday)
                {
                    if (item.Status == CMS.Common.Keywords.DataStatus.Display)
                    {
                        todayTotalOrderDisplay++;//Đếm đơn hàng đang thực hiện
                        #region Tạm tính tiền đối với các đơn hàng đang thực hiện
                        int totalminute = 0;
                        int minute = item.DateTimeStart.Value.Minute;
                        int hour = item.DateTimeStart.Value.Hour;
                        int minuteEnd = DateTime.Now.Minute;
                        int hourEnd = DateTime.Now.Hour;
                        totalminute = (hourEnd * 60 + minuteEnd) - (hour * 60 + minute);
                        if (totalminute < 0) { totalminute = 0; }
                        PriceBoardDao logicPriceBoard = new PriceBoardDao();
                        List<PriceBoard> listPriceBoard = new List<PriceBoard>();
                        listPriceBoard = logicPriceBoard.PriceBoardSearch(CMS.Common.Keywords.DataStatus.Display, totalminute);
                        if (listPriceBoard != null && listPriceBoard.Count > 0)
                        {
                            todayTotalPrice += listPriceBoard[0].Price.Value;
                        }
                        todayTotalPrice += item.Promotion.Value;
                        #endregion
                    }
                    if (item.Status == CMS.Common.Keywords.DataStatus.Success)
                    {
                        todayTotalOrderSuccess++;//Đếm đơn hàng đã thực hiện xong
                        todayTotalPrice += item.TotalPayment.Value + item.Promotion.Value;
                    }
                }
            }
            ViewBag.TodayTotalOrder = todayTotalOrder;
            ViewBag.TodayTotalOrderDisplay = todayTotalOrderDisplay;
            ViewBag.TodayTotalOrderSuccess = todayTotalOrderSuccess;
            ViewBag.TodayTotalPrice = String.Format("{0:n0}", todayTotalPrice);
            #endregion

            #region Tính tiền cho ngày hôm qua
            DateTime dStartYesterday = new DateTime(DateTime.Now.AddDays(-1).Year, DateTime.Now.AddDays(-1).Month, DateTime.Now.AddDays(-1).Day, 01, 01, 01);
            DateTime dEndYesterday = new DateTime(DateTime.Now.AddDays(-1).Year, DateTime.Now.AddDays(-1).Month, DateTime.Now.AddDays(-1).Day, 23, 59, 59);
            List<Order> listYesterday = new List<Order>();
            listYesterday = logic.OrderSearch("", dStartYesterday, dEndYesterday);
            int YesterdayTotalOrder = 0;
            int YesterdayTotalOrderDisplay = 0;
            int YesterdayTotalOrderSuccess = 0;
            double YesterdayTotalPrice = 0;
            if (listYesterday != null && listYesterday.Count > 0)
            {
                YesterdayTotalOrder = listYesterday.Count;
                foreach (var item in listYesterday)
                {
                    if (item.Status == CMS.Common.Keywords.DataStatus.Success)
                    {
                        YesterdayTotalOrderSuccess++;
                        YesterdayTotalPrice += item.TotalPayment.Value + item.Promotion.Value;
                    }
                    else if (item.Status == CMS.Common.Keywords.DataStatus.Display)
                    {
                        YesterdayTotalOrderDisplay++;
                    }
                }
            }
            ViewBag.YesterdayTotalOrder = YesterdayTotalOrder;
            ViewBag.YesterdayTotalOrderDisplay = YesterdayTotalOrderDisplay;
            ViewBag.YesterdayTotalOrderSuccess = YesterdayTotalOrderSuccess;
            ViewBag.YesterdayTotalPrice = String.Format("{0:n0}", YesterdayTotalPrice);
            #endregion

            ViewBag.Title = "Trang chủ";
            ViewData["Title"] = "Trang chủ";
            return View();
        }

        //
        // GET: /Admin/HomeManager/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Admin/HomeManager/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/HomeManager/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/HomeManager/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Admin/HomeManager/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/HomeManager/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Admin/HomeManager/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
