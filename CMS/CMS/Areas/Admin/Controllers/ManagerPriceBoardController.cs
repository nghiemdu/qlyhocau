﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;
using CMS.Common.StringProcess;

namespace CMS.Areas.Admin.Controllers
{
    public class ManagerPriceBoardController : BaseController
    {
        //
        // GET: /Admin/ManagerPriceBoard/

        public ActionResult Index()
        {
            PriceBoardDao logic = new PriceBoardDao();
            List<PriceBoard_GetByStatus_Result> model = new List<PriceBoard_GetByStatus_Result>();
            model = logic.PriceBoard_GetByStatus(CMS.Common.Keywords.DataStatus.Display).OrderBy(a => a.Value01).ToList();
            ViewBag.PanelTitle = ViewBag.Title = "Bảng giá";
            return View("Index", model);
        }

        //
        // GET: /Admin/ManagerPriceBoard/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Admin/ManagerPriceBoard/Create

        public ActionResult Create()
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thêm mới bảng giá";
            ViewBag.Value01 = new SelectList(BindListOrder(), "Value", "Text");
            PriceBoard item = new PriceBoard();
            item.TotalTimeStart = 0;
            item.TotalTimeEnd = 0;
            item.Price = 0;
            return View(item);
        }

        //
        // POST: /Admin/ManagerPriceBoard/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(PriceBoard item)
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thêm mới bảng giá";
            ViewBag.Value01 = new SelectList(BindListOrder(), "Value", "Text");
            try
            {
                // TODO: Add insert logic here
                string errorMess = "";

                if (item.TotalTimeStart == null)
                {
                    errorMess += "Thời gian bắt đầu không được để trống! ";
                }
                if (item.TotalTimeEnd == null)
                {
                    errorMess += "Thời gian kết thúc không được để trống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                PriceBoardDao logic = new PriceBoardDao();
                item.Status = 1;
                logic.PriceBoard_Insert(item);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Thêm mới bảng giá. {0} - {1}: {2} VNĐ", item.TotalTimeStart, item.TotalTimeEnd, item.Price));
                #endregion
                SetAlert("Thêm mới thành công!", "alert-success");
                return Redirect("/Admin/ManagerPriceBoard/Index/");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/ManagerPriceBoard/Edit/5

        public ActionResult Edit(string id)
        {
            id = Encryption.Decode(id);
            try
            {
                PriceBoardDao logic = new PriceBoardDao();
                PriceBoard item = new PriceBoard();
                item = logic.PriceBoard_ViewDetail(Convert.ToInt32(id));
                ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin bảng giá";
                ViewBag.Value01 = new SelectList(BindListOrder(), "Value", "Text", item.Value01);
                return View(item);
            }
            catch (Exception)
            {
                return Redirect("/Admin/ManagerPriceBoard/Index/");
            }
        }

        //
        // POST: /Admin/ManagerPriceBoard/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(string id, PriceBoard item)
        {
            ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin bảng giá";
            ViewBag.Value01 = new SelectList(BindListOrder(), "Value", "Text", item.Value01);
            try
            {
                // TODO: Add insert logic here
                string errorMess = "";
                if (item.TotalTimeStart == null)
                {
                    errorMess += "Thời gian bắt đầu không được để trống! ";
                }
                if (item.TotalTimeEnd == null)
                {
                    errorMess += "Thời gian kết thúc không được để trống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                PriceBoardDao logic = new PriceBoardDao();
                item.Id = Convert.ToInt32(Encryption.Decode(id));
                logic.PriceBoard_UpdateInfor(item);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật thông tin bảng giá. {0} - {1}: {2} VNĐ", item.TotalTimeStart, item.TotalTimeEnd, item.Price));
                #endregion
                SetAlert("Cập nhật thành công!", "alert-success");
                return Redirect("/Admin/ManagerPriceBoard/Index/");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/ManagerPriceBoard/Delete/5

        public ActionResult Delete(int id)
        {
            try
            {
                PriceBoardDao logic = new PriceBoardDao();
                PriceBoard item = new PriceBoard();
                item = logic.PriceBoard_ViewDetail(id);
                logic.PriceBoard_UpdateStatus(id, CMS.Common.Keywords.DataStatus.Deleted);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Xóa bảng giá. {0} - {1}: {2} VNĐ", item.TotalTimeStart, item.TotalTimeEnd, item.Price));
                #endregion
                SetAlert("Xóa thành công!", "alert-success");
                return Redirect("/Admin/ManagerPriceBoard/Index/");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // POST: /Admin/ManagerPriceBoard/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public List<SelectListItem> BindListOrder()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            for (int i = 1; i < 21; i++)
            {
                listItems.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            return listItems;
        }
    }
}
