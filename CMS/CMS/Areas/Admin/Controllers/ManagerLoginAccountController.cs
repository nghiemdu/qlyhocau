﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;
using CMS.Common.StringProcess;

namespace CMS.Areas.Admin.Controllers
{
    public class ManagerLoginAccountController : BaseController
    {
        //
        // GET: /Admin/ManagerLoginAccount/

        public ActionResult Index(string id)
        {
            string page = "1";
            const int pageSize = 50;
            string[] list = id.Split('-');
            page = id;
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            LoginAccountDao logic = new LoginAccountDao();
            List<LoginAccount_GetByStatus_Result> model = new List<LoginAccount_GetByStatus_Result>();
            List<LoginAccount_GetByStatus_Result> result = new List<LoginAccount_GetByStatus_Result>();
            model = logic.LoginAccount_GetByStatus(CMS.Common.Keywords.DataStatus.Display);
            ViewBag.Agency = model;
            ViewBag.PanelTitle = ViewBag.Title = "Danh sách tài khoản";
            int rowStart = 0;
            int rowEnd = 0;
            if (model != null && model.Count > 0)
            {
                rowStart = (Convert.ToInt32(page) - 1) * Convert.ToInt32(pageSize);
                rowEnd = (rowStart + Convert.ToInt32(pageSize)) > model.Count ? model.Count : rowStart + Convert.ToInt32(pageSize);
            }
            for (int i = rowStart; i < rowEnd; i++)
            {
                result.Add(model[i]);
            }
            ViewBag.TotalCount = model.Count;
            return View("Index", result);
        }

        //
        // GET: /Admin/ManagerLoginAccount/Details/5

        //
        // GET: /Admin/ManagerLoginAccount/Details/5

        public ActionResult Details()
        {
            LoginAccount itemLoginAccount = new LoginAccount();
            itemLoginAccount = (LoginAccount)ViewBag.ItemLoginAccount;
            try
            {
                LoginAccountDao logic = new LoginAccountDao();
                LoginAccount item = new LoginAccount();
                item = logic.LoginAccount_ViewDetail(Convert.ToInt32(itemLoginAccount.Id));
                ViewBag.PanelTitle = ViewBag.Title = "Thông tin tài khoản";
                ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text", item.GroupAccountId);
                return View(item);
            }
            catch (Exception)
            {
                return Redirect("/Admin/ManagerLoginAccount/Details");
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Details(LoginAccount item)
        {
            LoginAccount itemLoginAccount = new LoginAccount();
            itemLoginAccount = (LoginAccount)ViewBag.ItemLoginAccount;

            ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin tài khoản";
            ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text", item.GroupAccountId);
            try
            {
                // TODO: Add insert logic here
                string errorMess = "";

                if (item.FullName == "" || item.FullName == null)
                {
                    errorMess += "Tên tài khoản không được để trống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                LoginAccountDao logic = new LoginAccountDao();
                item.Id = itemLoginAccount.Id;
                item.LastUpdate = DateTime.Now;
                logic.LoginAccount_UpdateDetail(item);

                #region Insert history
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật thông tin tài khoản. ID: {0}, UserName: {1}", item.Id, item.UserName));
                #endregion
                SetAlert("Cập nhật thành công!", "alert-success");
                return Redirect("/Admin/ManagerLoginAccount/Details");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/ManagerLoginAccount/Create

        public ActionResult Create()
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thêm mới tài khoản";
            ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text");
            return View();
        }

        //
        // POST: /Admin/ManagerLoginAccount/Create

        [HttpPost]
        public ActionResult Create(LoginAccount item)
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thêm mới tài khoản";
            ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text");
            try
            {
                // TODO: Add insert logic here

                string errorMess = "";

                if (item.FullName == "" || item.FullName == null)
                {
                    errorMess += "Tên tài khoản không được để trống! ";
                }
                if (item.UserName == "" || item.UserName == null)
                {
                    errorMess += "UserName không được để trống! ";
                }
                if (CheckUserName(item.UserName, 0))
                {
                    errorMess += "Tên đăng nhập đã tồn tại trong hệ thống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }

                LoginAccountDao logic = new LoginAccountDao();
                item.Status = CMS.Common.Keywords.DataStatus.Display;
                item.Password = "12345678";
                item.DateTimeCreate = item.LastLogin = item.LastUpdate = DateTime.Now;
                logic.LoginAccount_Insert(item);
                #region Insert history
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Thêm mới tài khoản ID: {0}, UserName: {1}", item.Id, item.UserName));
                #endregion
                SetAlert("Thêm mới thành công!", "alert-success");
                return Redirect("/Admin/ManagerLoginAccount/Index/1");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/ManagerLoginAccount/Edit/5

        public ActionResult Edit(string id)
        {
            id = Encryption.Decode(id);
            try
            {
                LoginAccountDao logic = new LoginAccountDao();
                LoginAccount item = new LoginAccount();
                item = logic.LoginAccount_ViewDetail(Convert.ToInt32(id));
                ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin tài khoản";
                ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text", item.GroupAccountId);
                return View(item);
            }
            catch (Exception)
            {
                return Redirect("/Admin/ManagerLoginAccount/Index/1");
            }
        }

        //
        // POST: /Admin/ManagerLoginAccount/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(string id, LoginAccount item)
        {
            ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin tài khoản";
            ViewBag.GroupAccountId = new SelectList(BindListGroupAccount(), "Value", "Text", item.GroupAccountId);
            try
            {
                // TODO: Add insert logic here
                string errorMess = "";

                if (item.FullName == "" || item.FullName == null)
                {
                    errorMess += "Tên tài khoản không được để trống! ";
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                LoginAccountDao logic = new LoginAccountDao();
                item.Id = Convert.ToInt32(Encryption.Decode(id));
                item.LastUpdate = DateTime.Now;
                logic.LoginAccount_Update(item);

                #region Insert history
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDao HistoryDaoLogic = new HistoryDao();
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật thông tin tài khoản. ID: {0}, UserName: {1}", item.Id, item.UserName));
                #endregion
                SetAlert("Cập nhật thành công!", "alert-success");
                return Redirect("/Admin/ManagerLoginAccount/Index/1");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        public ActionResult ChangePass()
        {
            ViewBag.PanelTitle = ViewBag.Title = "Thay đổi mật khẩu";
            return View();
        }

        //
        // POST: /Admin/LoginAccount/Edit/5

        [HttpPost]
        public ActionResult ChangePass(FormCollection collection, CMS.Areas.Admin.Models.PassModel item)
        {
            LoginAccount itemLoginAccount = new LoginAccount();
            itemLoginAccount = (LoginAccount)ViewBag.ItemLoginAccount;
            ViewBag.PanelTitle = ViewBag.Title = "Thay đổi mật khẩu";
            try
            {
                LoginAccountDao logic = new LoginAccountDao();
                LoginAccount itemLoginLogic = new LoginAccount();
                itemLoginLogic = logic.LoginAccount_ViewDetail(itemLoginAccount.Id);
                // TODO: Add insert logic here
                string errorMess = "";
                if (item.oldPass == "" || item.oldPass == null)
                {
                    errorMess += "Mật khẩu cũ không được để trống! ";
                }
                else if (item.pass == "" || item.pass == null)
                {
                    errorMess += "Mật khẩu mới không được để trống! ";
                }
                else if (item.confirmPass == "" || item.confirmPass == null)
                {
                    errorMess += "Nhập lại mật khẩu không được để trống! ";
                }
                else
                {
                    if (itemLoginLogic.Password != Encryption.Md5Encrypt(item.oldPass))
                    {
                        errorMess += "Mật khẩu cũ không chính xác! ";
                    }
                    if (item.pass != item.confirmPass)
                    {
                        errorMess += "Mật khẩu và nhập lại mật khẩu phải trùng nhau! ";
                    }
                    if (item.pass.Length < 8)
                    {
                        errorMess += "Mật khẩu phải từ 8 ký tự trở lên! ";
                    }
                }
                if (errorMess != "")
                {
                    SetAlert(errorMess, "alert-danger");
                    return View();
                }
                logic.LoginAccount_ChangePassword(itemLoginAccount.Id, item.pass);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật mật khẩu. Username: {0}", itemLoginLogic.UserName));
                #endregion
                SetAlert("Cập nhật thành công!", "alert-success");
                return Redirect("/Admin/ManagerLoginAccount/Details/");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/ManagerLoginAccount/Delete/5

        public ActionResult ResetPass(string id)
        {
            try
            {
                id = CMS.Common.StringProcess.Encryption.Decode(id);
                LoginAccountDao logic = new LoginAccountDao();
                LoginAccount item = new LoginAccount();
                item = logic.LoginAccount_ViewDetail(Convert.ToInt32(id));
                logic.LoginAccount_ChangePassword(Convert.ToInt32(id), "12345678");
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Reset mật khẩu tài khoản Name: {0}", item.UserName));
                #endregion
                SetAlert("Reset mật khẩu thành công!", "alert-success");
                return Redirect("/Admin/ManagerLoginAccount/Index/1");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        // GET: /Admin/LoginAccount/Delete/5

        public ActionResult Delete(string id)
        {
            try
            {
                id = CMS.Common.StringProcess.Encryption.Decode(id);
                LoginAccountDao logic = new LoginAccountDao();
                LoginAccount item = new LoginAccount();
                item = logic.LoginAccount_ViewDetail(Convert.ToInt32(id));
                logic.LoginAccount_UpdateStatus(Convert.ToInt32(id), CMS.Common.Keywords.DataStatus.Deleted);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Xóa tài khoản Name: {0}", item.UserName));
                #endregion
                SetAlert("Xóa thành công tài khoản!", "alert-success");
                return Redirect("/Admin/ManagerLoginAccount/Index/1");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // POST: /Admin/ManagerLoginAccount/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public List<SelectListItem> BindListGroupAccount()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Nhân viên", Value = "2" });
            listItems.Add(new SelectListItem { Text = "Quản lý", Value = "3" });
            return listItems;
        }

        public bool CheckUserName(string userName, int id)
        {
            bool bReturn = false;
            LoginAccountDao logic = new LoginAccountDao();
            List<LoginAccount_GetByStatus_Result> list = new List<LoginAccount_GetByStatus_Result>();
            list = logic.LoginAccount_GetByStatus(CMS.Common.Keywords.DataStatus.Display);
            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    if (item.Id != id)//Không so sánh với chính tài khoản đó
                    {
                        if (item.UserName.ToLower() == userName.ToLower()) { bReturn = true; break; }
                    }
                }
            }
            return bReturn;
        }
    }
}
