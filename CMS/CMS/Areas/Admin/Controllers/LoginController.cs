﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;
using CMS.Common.StringProcess;

namespace CMS.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            ConfigDao ConfigLogic = new ConfigDao();
            if (ConfigLogic.Configs_ViewDetail(6).Content != null)
            {
                ViewBag.NumberMobile = ConfigLogic.Configs_ViewDetail(6).Content;//Lấy số điện thoại
            }
            else { ViewBag.NumberMobile = ""; }
            if (ConfigLogic.Configs_ViewDetail(5).Content != null)
            {
                ViewBag.Email = ConfigLogic.Configs_ViewDetail(5).Content.Trim();//Lấy email
            }
            else { ViewBag.Email = ""; }
            return View();
        }
        public ActionResult Login(CMS.Areas.Admin.Models.LoginModel model)
        {
            #region Config
            ConfigDao ConfigLogic = new ConfigDao();
            //if (ConfigLogic.Configs_ViewDetail(13).Content != null)
            //{
            //    ViewBag.Logo = ConfigLogic.Configs_ViewDetail(13).Content;//Lấy logo
            //}
            //else { ViewBag.Logo = ""; }
            if (ConfigLogic.Configs_ViewDetail(6).Content != null)
            {
                ViewBag.NumberMobile = ConfigLogic.Configs_ViewDetail(6).Content;//Lấy số điện thoại
            }
            else { ViewBag.NumberMobile = ""; }
            //if (ConfigLogic.Configs_ViewDetail(7).Content != null)
            //{
            //    ViewBag.TruSo = ConfigLogic.Configs_ViewDetail(7).Content.Trim();//Trụ sở làm việc
            //}
            //else { ViewBag.TruSo = ""; }
            if (ConfigLogic.Configs_ViewDetail(5).Content != null)
            {
                ViewBag.Email = ConfigLogic.Configs_ViewDetail(5).Content.Trim();//Lấy email
            }
            else { ViewBag.Email = ""; }
            #endregion
            LoginAccountDao logic = new LoginAccountDao();
            if (ModelState.IsValid)
            {
                string UserName = model.UserName.Trim();
                string Password = model.Password.Trim();
                if (logic.LoginAccount_CheckLogin(UserName, Password))
                {
                    LoginAccount LoginAccountDetail = logic.LoginAccount_GetByUserNameAndPassword(UserName, Password);
                    if (LoginAccountDetail.Status == CMS.Common.Keywords.DataStatus.Deleted || LoginAccountDetail.Status == CMS.Common.Keywords.DataStatus.DeletedPermanently)
                    {
                        ModelState.AddModelError("", "Tài khoản của bạn đã bị xóa.");
                        return View("Index");
                    }
                    //Session[CMS.Common.Keywords.SystemSession.accountLoggedIn] = LoginAccountDetail;
                    logic.LoginAccount_UpdateLastLogin(LoginAccountDetail.Id, DateTime.Now);
                    #region Lưu cookie
                    Response.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value = CMS.Common.StringProcess.Encryption.Encode(LoginAccountDetail.Id.ToString());
                    #endregion
                    #region Insert history
                    HistoryDao HistoryDaoLogic = new HistoryDao();
                    HistoryDaoLogic.Insert(LoginAccountDetail.Id, string.Format("({0}) Tài khoản: {1} đăng nhập thành công.", DateTime.Now, LoginAccountDetail.FullName));
                    #endregion
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");
                }
            }
            return View("Index");
        }
        [HttpGet]
        public ActionResult Logout()
        {
            //Session[CMS.Common.Keywords.SystemSession.accountLoggedIn] = null;
            Response.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value = null;
            return Redirect(CMS.Common.Keywords.Domain.domain);
        }
    }
}