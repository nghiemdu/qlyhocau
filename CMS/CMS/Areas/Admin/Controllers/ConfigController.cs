﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Model;
using CMS.Dao;
using CMS.Lib;
using CMS.Common.StringProcess;

namespace CMS.Areas.Admin.Controllers
{
    public class ConfigController: BaseController
    {
        //
        // GET: /Admin/Config/

        public ActionResult Index()
        {
            string order = "4,5,6,7,13,14,12,8,10,11,1,2,3,9";
            string[] list = order.Split(',');
            ConfigDao ConfigLogic = new ConfigDao();
            List<Config_GetByStatus_Result> model = new List<Config_GetByStatus_Result>();
            List<Config_GetByStatus_Result> listItem = new List<Config_GetByStatus_Result>();
            model = ConfigLogic.Config_GetByStatus(CMS.Common.Keywords.DataStatus.Display);
            foreach (var item in list)
            {
                foreach (var i in model)
                {
                    if (item == i.Id.ToString())
                    {
                        listItem.Add(i);
                        break;
                    }
                }
            }
            ViewBag.PanelTitle = ViewBag.Title = "Danh sách các thiết lập hệ thống";
            return View("Index", listItem);
        }

        //
        // GET: /Admin/Config/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Admin/Config/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/Config/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/Config/Edit/5

        public ActionResult Edit(string id)
        {
            id = Encryption.Decode(id);
            try
            {
                ConfigDao ConfigLogic = new ConfigDao();
                Config item = new Config();
                item = ConfigLogic.Configs_ViewDetail(Convert.ToInt32(id));
                ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin thiết lập hệ thống: " + item.Name;
                ViewBag.Id = id;
                return View(item);
            }
            catch (Exception)
            {
                return Redirect("/Admin/Config/Index");
            }
        }

        //
        // POST: /Admin/Config/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(string id, FormCollection collection, Config item)
        {
            ViewBag.PanelTitle = ViewBag.Title = "Cập nhật thông tin thiết lập hệ thống: " + item.Name;
            try
            {
                // TODO: Add insert logic here
                //string errorMess = "";
                //if (item.Content == "" || item.Content == null)
                //{
                //    errorMess += "Nội dung của thiết lập không được để trống! ";
                //}
                //if (errorMess != "")
                //{
                //    SetAlert(errorMess, "alert-danger");
                //    return View();
                //}

                ConfigDao ConfigLogic = new ConfigDao();
                item.Id = Convert.ToInt32(Encryption.Decode(id));
                ConfigLogic.Configs_Update(item);
                #region Insert history
                HistoryDao HistoryDaoLogic = new HistoryDao();
                int loginId = Convert.ToInt32(CMS.Common.StringProcess.Encryption.Decode(Request.Cookies[CMS.Common.Keywords.SystemSession.accountLoggedIn].Value));
                HistoryDaoLogic.Insert(loginId, string.Format("Cập nhật thông tin thiết lập hệ thống. Name: {0}", item.Name));
                #endregion
                SetAlert("Cập nhật thành công thiết lập hệ thống!", "alert-success");
                return Redirect("/Admin/Config/Index");
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi " + ex, "alert-danger");
                return View();
            }
        }

        //
        // GET: /Admin/Config/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Admin/Config/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}