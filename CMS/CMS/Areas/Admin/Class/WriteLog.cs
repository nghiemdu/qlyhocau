﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
namespace CMS.Areas.Admin.Class
{
    public class WriteLog
    {
        static StreamWriter VietFileText;
        public static string Current_DATE(string Date_type)
        {
            DateTime dt = System.DateTime.Now;
            string F_Time = dt.ToShortDateString();
            switch (Date_type.ToLower())
            {
                case "yyyy_mm_dd":
                    F_Time = So_chuan(dt.Year).ToString() + "_" + So_chuan(dt.Month).ToString() + "_" + So_chuan(dt.Day).ToString();
                    break;
                case "yyyymmdd":
                    F_Time = So_chuan(dt.Year).ToString() + So_chuan(dt.Month).ToString() + So_chuan(dt.Day).ToString();
                    break;
                case "dd/mm/yyyy hh:pp:ss":
                    F_Time = So_chuan(dt.Day).ToString() + "/" + So_chuan(dt.Month).ToString() + "/" + So_chuan(dt.Year).ToString() + " " + So_chuan(dt.Hour).ToString() + ":" + So_chuan(dt.Minute).ToString() + ":" + So_chuan(dt.Second).ToString();
                    break;
                case "hh:mm:ss:ms":
                    F_Time = So_chuan(dt.Hour).ToString() + ":" + So_chuan(dt.Minute).ToString() + ":" + So_chuan(dt.Second).ToString() + ":" + Bon_So_chuan(dt.Millisecond.ToString()).ToString();
                    break;
                //case "HH:mm:ss":
                case "hh:mm:ss":
                    F_Time = So_chuan(dt.Hour).ToString() + ":" + So_chuan(dt.Minute).ToString() + ":" + So_chuan(dt.Second).ToString();
                    break;
                //case "dd/MM/yyyy":
                case "dd/mm/yyyy":
                    F_Time = So_chuan(dt.Day).ToString() + "/" + So_chuan(dt.Month).ToString() + "/" + So_chuan(dt.Year).ToString();
                    break;
            }
            return F_Time;
        }

        public static string Bon_So_chuan(string so)
        {
            string kq = "";
            try
            {
                int l = so.Length;
                switch (l)
                {
                    case 1:
                        kq = "000" + so.ToString();
                        break;
                    case 2:
                        kq = "00" + so.ToString();
                        break;
                    case 3:
                        kq = "0" + so.ToString();
                        break;
                    case 4:
                        kq = so.ToString();
                        break;
                }
                return kq.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string So_chuan(double so)
        {
            try
            {
                string so1 = "";
                if (so < 10)
                {
                    so1 = "0" + so.ToString();
                }
                else
                {
                    so1 = so.ToString();
                }
                return so1.ToString();
            }
            catch (Exception)
            {
                return "";
            }

        }

        public static void CreateLogFile(string Log_type)
        {
            string File_name = Current_DATE("yyyymmdd");
            string v_AppPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            string log_folder = v_AppPath + "Logs";
            string file_path = "";
            if (Log_type == "LOG")
            {
                file_path = log_folder + "\\" + File_name + "_LOG.log";
            }
            else if (Log_type == "ERR")
            {
                file_path = log_folder + "\\" + File_name + "_ERR.log";
            }
            if (System.IO.File.Exists(file_path) == false)
            {
                if (Directory.Exists(log_folder) == false)
                {
                    Directory.CreateDirectory(log_folder);
                }
                VietFileText = System.IO.File.CreateText(file_path);
                VietFileText.Close();
            }
        }

        public static void UpdateLog(string pv_logtype, string p_Sukien, string p_hamgoi, string Err_str)
        {
            string File_name = Current_DATE("yyyymmdd");
            string v_AppPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            string log_folder = v_AppPath + "Logs";
            string file_path = "";
            if (pv_logtype == "LOG")
            {
                file_path = log_folder + "\\" + File_name + "_LOG.log";
            }
            else if (pv_logtype == "ERR")
            {
                file_path = log_folder + "\\" + File_name + "_ERR.log";
            }

            if (System.IO.File.Exists(file_path) == false)
            {
                CreateLogFile(pv_logtype);
            }
            //--------------------------------------
            VietFileText = System.IO.File.AppendText(file_path);
            VietFileText.WriteLine("=======" + Current_DATE("dd/mm/yyyy hh:pp:ss") + "========================================================");
            VietFileText.WriteLine("  Sự kiện : " + p_Sukien);
            VietFileText.WriteLine("  Hàm gọi : " + p_hamgoi);
            VietFileText.WriteLine("  Mess: " + Err_str);
            VietFileText.WriteLine("  ");
            VietFileText.Close();
        }
    }
}