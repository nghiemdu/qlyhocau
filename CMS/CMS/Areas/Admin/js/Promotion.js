﻿function AddAgency(id) {
    //alert(id);
    var listId = $('#Agency').val();
    if (listId.includes(id)) {
        listId = listId.replace(id, "");
        var element = document.getElementById("row_" + id.replace("[", "").replace("]", ""));
        element.classList.remove("active");
    }
    else {
        listId = listId + id;
        var element = document.getElementById("row_" + id.replace("[", "").replace("]", ""));
        element.classList.add("active");
    }
    document.getElementById("Agency").value = listId;
    listId = replaceAll(listId,"][","," );
    const myArray = listId.split(",");
    document.getElementById("AgencyCount").value = myArray.length;
}
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}