﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace CMS.Areas.Admin.Models
{
    public class OrderModel
    {
        public string orderText { set; get; }
        public int orderValue { set; get; }
    }
}