﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Areas.Admin.Models
{
    public class PermissionModel
    {
        public string PermissionName { set; get; }
        public string ControllerName { set; get; }
    }
}