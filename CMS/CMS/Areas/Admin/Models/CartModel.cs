﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CMS.Areas.Admin.Models
{
    public class CartModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public int Number { set; get; }
        public string Price{ set; get; }
        public string TotalPrice{ set; get; }
    }
}