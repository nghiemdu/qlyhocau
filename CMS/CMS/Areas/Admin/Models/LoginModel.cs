﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CMS.Areas.Admin.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Phải nhập Username")]
        public string UserName { set; get; }
        [Required(ErrorMessage = "Phải nhập Password")]
        public string Password { set; get; }
    }
}