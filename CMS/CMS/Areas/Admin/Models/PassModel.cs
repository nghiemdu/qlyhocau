﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace CMS.Areas.Admin.Models
{
    public class PassModel
    {
        public string oldPass { set; get; }
        public string pass { set; get; }
        public string confirmPass{ set; get; }
    }
}