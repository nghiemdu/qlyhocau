﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using CMS.Models;
using CMS.Dao;
using CMS.Model;

namespace CMS.Lib
{
    public static class PermissionFunction
    {
        public static int checkPermissionGroupAccount(int idUser)
        {
            LoginAccountDao LoginLogic = new LoginAccountDao();
            LoginAccount item = new LoginAccount();
            item = LoginLogic.LoginAccount_GetById(idUser);
            if (item.GroupAccountId == 1)
            {
                return 1;
            }
            return 0;
        }
        public static int checkPermissionGroupAccountInLessonDetail(int idUser, string idLessonDetail)
        {
            LoginAccountDao LoginLogic = new LoginAccountDao();
            GroupAccountDao GroupAccountLogic = new GroupAccountDao();
            LoginAccount item = new LoginAccount();
            item = LoginLogic.LoginAccount_GetById(idUser);
            GroupAccount_GetByPK_Result itemGroupAccount = new GroupAccount_GetByPK_Result();
            itemGroupAccount = GroupAccountLogic.GroupAccount_GetById(item.GroupAccountId.Value);
            string[] listAuthority = itemGroupAccount.Authority.Split(',');
            foreach (var itemAuthority in listAuthority)
            {
                if (idLessonDetail == itemAuthority)
                {
                    return 1;
                }
            }
            return 0;
        }
    }
}