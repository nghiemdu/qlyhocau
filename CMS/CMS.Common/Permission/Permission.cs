﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using CMS.Model;


namespace CMS.Common.Permission
{
    public class Permission
    {
        public const string Home = "Home";
        public const string HomeManager = "HomeManager";

        public const string ManagerLoginAccount = "ManagerLoginAccount";
        public const string LoginAccount = "LoginAccount";

        public const string ManagerPriceBoard = "ManagerPriceBoard";
        public const string PriceBoard = "PriceBoard";

        public const string ManagerOrder = "ManagerOrder";
        public const string Order = "Order";

        public const string ManagerReport = "ManagerReport";

        public const string Config = "Config";

        public static Dictionary<string, string> FunctionList = new Dictionary<string, string>()
        {
            {Home,"Trang chủ tổng đại lý"},
            {HomeManager,"Trang chủ quản lý đại lý"},

            {ManagerLoginAccount,"Quản lý tài khoản (Quản lý)"},
            {LoginAccount,"Quản lý tài khoản (Nhân viên)"},

            {ManagerPriceBoard,"Quản lý bảng giá (Quản lý)"},
            {PriceBoard,"Quản lý bảng giá (Nhân viên)"},

            {ManagerOrder,"Quản lý đơn hàng (Quản lý)"},
            {Order,"Quản lý đơn hàng (Nhân viên)"},

            {ManagerReport, "Quản lý báo cáo (Quản lý)"},

            {Config,"Quản lý cài đặt hệ thống"},
        };

        public static Dictionary<string, string> ListPermissionController = new Dictionary<string, string>()
        {
            {Home,"Home"},
            {HomeManager,"HomeManager"},

            {ManagerLoginAccount,"ManagerLoginAccount, GroupAccount"},
            {LoginAccount,"LoginAccount"},

            {ManagerReport, "ManagerReport"},

            {ManagerPriceBoard, "ManagerPriceBoard"},
            {PriceBoard, "PriceBoard"},

            {ManagerOrder, "ManagerOrder"},
            {Order, "Order"},

            {Config,"Historyr,Config"},
        };
    }
}


