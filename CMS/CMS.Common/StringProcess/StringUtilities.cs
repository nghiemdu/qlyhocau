﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Common.StringProcess
{
    public class StringUtilities
    {
        private static readonly string[] VietnameseSigns = new string[]
        {
        "aAeEoOuUiIdDyY",
        "áàạảãâấầậẩẫăắằặẳẵ",
        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
        "éèẹẻẽêếềệểễ",
        "ÉÈẸẺẼÊẾỀỆỂỄ",
        "óòọỏõôốồộổỗơớờợởỡ",
        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
        "úùụủũưứừựửữ",
        "ÚÙỤỦŨƯỨỪỰỬỮ",
        "íìịỉĩ",
        "ÍÌỊỈĨ",
        "đ",
        "Đ",
        "ýỳỵỷỹ",
        "ÝỲỴỶỸ"
        };

        public static string RemoveSign4VietnameseString(string str)
        {
            //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str;
        }

        #region Check Unicode string
        public static bool GetDataCoding(string text)
        {
            if (UTF2Endian(text).Length != ASCII2Endian(text).Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static string UTF2Endian(string s)
        {

            Encoding ui = Encoding.BigEndianUnicode;

            Encoding u8 = Encoding.UTF8;

            return ui.GetString(u8.GetBytes(s));

        } // u2i

        static string ASCII2Endian(string s)
        {
            Encoding ui = Encoding.BigEndianUnicode;

            Encoding a = Encoding.ASCII;

            return ui.GetString(a.GetBytes(s));

        } // a2i
        #endregion

        public static bool IsNumeric(string str)
        {
            foreach (char c in str)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
