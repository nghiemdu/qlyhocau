﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
namespace CMS.Common.StringProcess
{
    public class Encryption
    {
        #region Variables
        private static string EncryptionKeyStart = "D$5GH67J*";//Không được thay đổi
        #endregion

        #region Tạo mảng byte chuỗi mã hóa
        /// <summary>
        /// Tạo mảng byte chuỗi mã hóa
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        static byte[] encryptData(string data)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashedBytes;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(data));
            return hashedBytes;
        }
        #endregion

        #region Mã hóa chuỗi dưới dạng md5
        /// <summary>
        /// Mã hóa chuỗi dưới dạng md5
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string Md5Encrypt(string data)
        {
            return BitConverter.ToString(encryptData(data)).Replace("-", "").ToLower();
        }
        #endregion

        #region Tạo một chuỗi ngẫu nhiên
        /// <summary>
        /// Tạo một chuỗi ngẫu nhiên
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string RandomString(int size)
        {
            Random rnd = new Random();
            string srds = "";
            string[] str = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            for (int i = 0; i < size; i++)
            {
                srds = srds + str[rnd.Next(0, 61)];
            }
            return srds;
        }
        #endregion

        #region[Encode for URL]
        public static string Encode(string src)
        {
            byte[] b;
            try
            {
                b = Encoding.Unicode.GetBytes(EncryptionKeyStart + src);
            }
            catch { return src; }
            return Convert.ToBase64String(b);

        }
        public static string Decode(string src)
        {
            byte[] b;
            try
            {
                b = Convert.FromBase64String(src);
                src = Encoding.Unicode.GetString(b);
                // src = src.Substring(0, src.Length - 9);
                src = src.Remove(0, 9);
            }
            catch { return src; }
            return src;

        }
        #endregion
    }
}
