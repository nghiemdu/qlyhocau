﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Common.Keywords
{
    public class SystemSession
    {
        public const string accountLoggedIn = "accountLoggedIn";
        public const string sessionShoppingCart = "sessionShoppingCart";
        public const string teacherLoggedIn = "teacherLoggedIn";
        public const string cartSession = "CartSession";
    }
}
