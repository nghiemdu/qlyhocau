﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Common.Keywords
{
    public class DataStatus
    {
        public const int Disable = 0;
        public const int Display = 1;
        public const int Deleted = -1;
        public const int DeletedPermanently = -2;//Xóa vĩnh viễn
        public const int Success = 3;//Đã thành công
        public const int Transport = 4;//Đang chuyển đến người nhận thẻ. Người nhận thẻ chưa ấn nhận hàng
        public const int NotExtend = -3;//KHông gia hạn
    }
}
