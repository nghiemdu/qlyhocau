﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using CMS.Model;

namespace CMS.Common.Utility
{
    public class Utility
    {
        private static string[] ChuSo = new string[10] { " không", " một", " hai", " ba", " bốn", " năm", " sáu", " bẩy", " tám", " chín" };
        private static string[] Tien = new string[6] { "", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ" };
        #region Convert file name
        public static string ChangeAv(string ipStrChange)
        {
            if (ipStrChange != null)
            {
                ipStrChange = ipStrChange.ToLower();

                ipStrChange = ipStrChange.Replace('"', '-');
                var vRegRegex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                var vStrFormD = ipStrChange.Normalize(NormalizationForm.FormD);
                string sReturn = "";
                sReturn= vRegRegex.Replace(vStrFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D')
                    .Replace(" ", "-").Replace(".", "").Replace(":", "").Replace("/", "-").Replace("--", "-").Replace(",", "")
                    .Replace("\"", "").Replace("?", "").Replace("&", "").Replace("-p", "-").Replace("-lp", "-")
                    .Replace("-n", "-").Replace("-ln", "-").Replace("---", "").Replace("+", "").Replace("lt", "");
                sReturn = sReturn.Replace("---", "").Replace("–-", "").Replace("--", "");
                return sReturn;
            }
            else return "";
        }
        public static string ChangeAvSearch(string ipStrChange)
        {
            ipStrChange = ipStrChange.Replace('"', ' ');
            var vRegRegex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            var vStrFormD = ipStrChange.Normalize(NormalizationForm.FormD);
            return vRegRegex.Replace(vStrFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace(" ", " ").Replace(".", "").Replace(":", " ").Replace("/", " ").Replace("?", "");
        }
        public static string ConvertChangeAvSearch(string ipStrChange)
        {
            ipStrChange = ipStrChange.Replace('-', ' ');
            return ipStrChange;
        }
        #endregion
        #region Kiểm tra số chẵn lẻ
        public static bool CheckEvenOrOdd(decimal price)
        {
            string strSoTien = price.ToString();
            string[] num = strSoTien.Split('.');
            if (num.Length == 1)
                return false;
            if (num.Length == 2 && Convert.ToInt32(num[1]) == 0)
                return false;
            return true;
        }
        #endregion
        #region Hàm đọc số thành chữ
        public static string PriceInWords(decimal SoTien, string strTail)
        {
            int lan, i;
            decimal so;
            string result = "";
            string KetQua = "", tmp = "";
            bool booAm = false;
            int[] ViTri = new int[6];

            if (SoTien < 0)
            {
                SoTien = -SoTien;
                booAm = true;
            }
            if (SoTien == 0) return "Không đồng !";
            if (SoTien > 0)
                so = SoTien;
            else
                so = -SoTien;

            //Kiểm tra số quá lớn
            if (SoTien > 8999999999999999)
            {
                SoTien = 0;
                return "Số tiền quá lớn";
            }

            if (CheckEvenOrOdd(SoTien) == false)
            {
                ViTri[5] = (int)(so / 1000000000000000);
                so = so - long.Parse(ViTri[5].ToString()) * 1000000000000000;
                ViTri[4] = (int)(so / 1000000000000);
                so = so - long.Parse(ViTri[4].ToString()) * +1000000000000;
                ViTri[3] = (int)(so / 1000000000);
                so = so - long.Parse(ViTri[3].ToString()) * 1000000000;
                ViTri[2] = (int)(so / 1000000);
                ViTri[1] = (int)((so % 1000000) / 1000);
                ViTri[0] = (int)(so % 1000);

                if (ViTri[5] > 0)
                    lan = 5;
                else if (ViTri[4] > 0)
                    lan = 4;
                else if (ViTri[3] > 0)
                    lan = 3;
                else if (ViTri[2] > 0)
                    lan = 2;
                else if (ViTri[1] > 0)
                    lan = 1;
                else
                    lan = 0;

                for (i = lan; i >= 0; i--)
                {
                    tmp = DocSo3ChuSo(ViTri[i]);
                    KetQua += tmp;
                    if (ViTri[i] != 0) KetQua += Tien[i];
                }

                if (KetQua.Substring(KetQua.Length - 1, 1) == ",")
                    KetQua = KetQua.Substring(0, KetQua.Length - 1);

                KetQua = KetQua.Trim() + strTail;

                if (booAm)
                    KetQua = string.Format("Âm {0}{1}", KetQua.Trim(), strTail);

                result += KetQua.Substring(0, 1).ToLower() + KetQua.Substring(1);
                return result.Substring(0, 1).ToUpper() + result.Substring(1);
            }
            else
            {
                ViTri[5] = (int)(so / 1000000000000000);
                so = so - long.Parse(ViTri[5].ToString()) * 1000000000000000;
                ViTri[4] = (int)(so / 1000000000000);
                so = so - long.Parse(ViTri[4].ToString()) * +1000000000000;
                ViTri[3] = (int)(so / 1000000000);
                so = so - long.Parse(ViTri[3].ToString()) * 1000000000;
                ViTri[2] = (int)(so / 1000000);
                ViTri[1] = (int)((so % 1000000) / 1000);
                ViTri[0] = (int)(so % 1000);

                if (ViTri[5] > 0)
                    lan = 5;
                else if (ViTri[4] > 0)
                    lan = 4;
                else if (ViTri[3] > 0)
                    lan = 3;
                else if (ViTri[2] > 0)
                    lan = 2;
                else if (ViTri[1] > 0)
                    lan = 1;
                else
                    lan = 0;

                for (i = lan; i >= 0; i--)
                {
                    tmp = DocSo3ChuSo(ViTri[i]);
                    KetQua += tmp;
                    if (ViTri[i] != 0) KetQua += Tien[i];
                }

                if (KetQua.Substring(KetQua.Length - 1, 1) == ",")
                    KetQua = KetQua.Substring(0, KetQua.Length - 1);

                KetQua = KetQua.Trim();

                if (booAm)
                    KetQua = string.Format("Âm {0}{1}", KetQua.Trim(), strTail);

                string[] num = SoTien.ToString().Split('.');
                result += string.Format("{0} lẻ {1}", KetQua.Trim(), PriceInWords(Convert.ToDecimal(num[1]), " đồng"));
                return result.Substring(0, 1).ToUpper() + result.Substring(1);
            }
        }
        #endregion
        #region Hàm đọc số có 3 chữ số
        private static string DocSo3ChuSo(int baso)
        {
            int tram, chuc, donvi;
            string KetQua = "";
            tram = (int)(baso / 100);
            chuc = (int)((baso % 100) / 10);
            donvi = baso % 10;

            if ((tram == 0) && (chuc == 0) && (donvi == 0))
                return "";

            if (tram != 0)
            {
                KetQua += ChuSo[tram] + " trăm";
                if ((chuc == 0) && (donvi != 0)) KetQua += " linh";
            }

            if ((chuc != 0) && (chuc != 1))
            {
                KetQua += ChuSo[chuc] + " mươi";
                if ((chuc == 0) && (donvi != 0)) KetQua = KetQua + " linh";
            }

            if (chuc == 1)
                KetQua += " mười";

            switch (donvi)
            {
                case 1:
                    if ((chuc != 0) && (chuc != 1))
                    {
                        KetQua += " mốt";
                    }
                    else
                    {
                        KetQua += ChuSo[donvi];
                    }
                    break;
                case 5:
                    if (chuc == 0)
                    {
                        KetQua += ChuSo[donvi];
                    }
                    else
                    {
                        KetQua += " lăm";
                    }
                    break;
                default:
                    if (donvi != 0)
                    {
                        KetQua += ChuSo[donvi];
                    }
                    break;
            }
            return KetQua;
        }
        #endregion
        #region Resize ảnh
        public static System.Drawing.Image ResizeByWidth(System.Drawing.Image img, int width)
        {
            // lấy chiều rộng và chiều cao ban đầu của ảnh
            int originalW = img.Width;
            int originalH = img.Height;

            // lấy chiều rộng và chiều cao mới tương ứng với chiều rộng truyền vào của ảnh (nó sẽ giúp ảnh của chúng ta sau khi resize vần giứ được độ cân đối của tấm ảnh
            int resizedW = width;
            int resizedH = (originalH * resizedW) / originalW;

            // tạo một Bitmap có kích thước tương ứng với chiều rộng và chiều cao mới
            Bitmap bmp = new Bitmap(resizedW, resizedH);

            // tạo mới một đối tượng từ Bitmap
            Graphics graphic = Graphics.FromImage((System.Drawing.Image)bmp);
            graphic.InterpolationMode = InterpolationMode.High;

            // vẽ lại ảnh với kích thước mới
            graphic.DrawImage(img, 0, 0, resizedW, resizedH);

            // gải phóng resource cho đối tượng graphic
            graphic.Dispose();

            // trả về anh sau khi đã resize
            return (System.Drawing.Image)bmp;
        }
        #endregion
        #region Cắt chữ
        public static string WordCut(string text, int stringLenght)
        {
            string sReturn = "";
            if (text == null) return "";
            if (text.Length < stringLenght)
            {
                sReturn = text;
            }
            else
            {
                for (int i = stringLenght; i < (stringLenght + 50); i++)
                {
                    try
                    {
                        if (text[i] == ' ')
                        {
                            sReturn = text.Substring(0, i) + "...";
                            break;
                        }
                    }
                    catch
                    {
                        sReturn = text + "...";
                        break;
                    }

                }
            }
            return sReturn;
        }
        public static string WordCut(string text, int stringLenght, bool flag)
        {
            string sReturn = "";
            if (text == null) return "";
            if (text.Length < stringLenght)
            {
                sReturn = text;
            }
            else
            {
                for (int i = stringLenght; i < (stringLenght + 50); i++)
                {
                    sReturn = text.Substring(0, i) + "...";
                    break;
                }
            }
            return sReturn;
        }
        #endregion
        #region Đọc tháng
        public static string GetMonth(int month)
        {
            string sReturn = "";
            switch (month)
            {
                case 1:
                    sReturn = "Tháng 01";
                    break;
                case 2:
                    sReturn = "Tháng 02";
                    break;
                case 3:
                    sReturn = "Tháng 03";
                    break;
                case 4:
                    sReturn = "Tháng 04";
                    break;
                case 5:
                    sReturn = "Tháng 05";
                    break;
                case 6:
                    sReturn = "Tháng 06";
                    break;
                case 7:
                    sReturn = "Tháng 07";
                    break;
                case 8:
                    sReturn = "Tháng 08";
                    break;
                case 9:
                    sReturn = "Tháng 09";
                    break;
                case 10:
                    sReturn = "Tháng 10";
                    break;
                case 11:
                    sReturn = "Tháng 11";
                    break;
                case 12:
                    sReturn = "Tháng 12";
                    break;

            }
            return sReturn;
        }
        #endregion
        #region Lấy ngày cuối cùng của tháng
        /// <summary>
        /// Lấy ra ngày cuối cùng trong tháng có chứa
        /// 1 ngày bất kỳ được truyền vào
        /// </summary>
        /// <param name="dtInput">Ngày nhập vào</param>
        /// <returns>Ngày cuối cùng trong tháng</returns>
        public static DateTime GetLastDayOfMonth(DateTime dtInput)
        {
            dtInput = new DateTime(dtInput.Year, dtInput.Month, dtInput.Day);
            DateTime dtResult = dtInput;
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-(dtResult.Day));
            return dtResult;
        }
        /// <summary>
        /// Lấy ra ngày cuối cùng trong tháng được truyền vào
        /// là 1 số nguyên từ 1 đến 12
        /// </summary>
        /// <param name="iMonth"></param>
        /// <returns></returns>
        public static DateTime GetLastDayOfMonth(int iMonth)
        {
            DateTime dtResult = new DateTime(DateTime.Now.Year, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-(dtResult.Day));
            return dtResult;
        }
        #endregion
        #region lấy ngày đầu tiên và cuối cùng của tuần
        public static DateTime MondayOfWeek(DateTime date)
        {
            date = new DateTime(date.Year, date.Month, date.Day);
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

        public static DateTime SundayOfWeek(DateTime date)
        {
            date = new DateTime(date.Year, date.Month, date.Day);
            return MondayOfWeek(date).AddDays(6);
        }
        #endregion
        #region lấy ngày cuối cùng của quý
        public static DateTime GetLastDayOfQuarter(DateTime dtInput)
        {
            dtInput = new DateTime(dtInput.Year, dtInput.Month, dtInput.Day);
            DateTime dtResult = dtInput;
            int months = dtInput.Month;
            if (months == 1 || months == 2 || months == 3) { dtResult = GetLastDayOfMonth(3); }
            if (months == 4 || months == 5 || months == 6) { dtResult = GetLastDayOfMonth(6); }
            if (months == 7 || months == 8 || months == 9) { dtResult = GetLastDayOfMonth(9); }
            if (months == 10 || months == 11 || months == 12) { dtResult = GetLastDayOfMonth(12); }
            return dtResult;
        }
        public static DateTime GetFirtDayOfQuarter(DateTime dtInput)
        {
            dtInput = new DateTime(dtInput.Year, dtInput.Month, dtInput.Day);
            DateTime dtResult = dtInput;
            int months = dtInput.Month;
            if (months == 1 || months == 2 || months == 3) { dtResult = Convert.ToDateTime(DateTime.Now.Year + "-01-01"); }
            if (months == 4 || months == 5 || months == 6) { dtResult = Convert.ToDateTime(DateTime.Now.Year + "-01-04"); }
            if (months == 7 || months == 8 || months == 9) { dtResult = Convert.ToDateTime(DateTime.Now.Year + "-01-07"); }
            if (months == 10 || months == 11 || months == 12) { dtResult = Convert.ToDateTime(DateTime.Now.Year + "-01-10"); }
            return dtResult;
        }
        #endregion
    }
}
