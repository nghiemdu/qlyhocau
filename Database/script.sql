USE [master]
GO
/****** Object:  Database [QLyHoCau]    Script Date: 21/10/2022 3:49:05 PM ******/
CREATE DATABASE [QLyHoCau]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLyHoCau', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\QLyHoCau.mdf' , SIZE = 11264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLyHoCau_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\QLyHoCau_log.ldf' , SIZE = 6272KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLyHoCau] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLyHoCau].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLyHoCau] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLyHoCau] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLyHoCau] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLyHoCau] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLyHoCau] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLyHoCau] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLyHoCau] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [QLyHoCau] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLyHoCau] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLyHoCau] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLyHoCau] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLyHoCau] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLyHoCau] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLyHoCau] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLyHoCau] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLyHoCau] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLyHoCau] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLyHoCau] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLyHoCau] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLyHoCau] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLyHoCau] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLyHoCau] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLyHoCau] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLyHoCau] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLyHoCau] SET  MULTI_USER 
GO
ALTER DATABASE [QLyHoCau] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLyHoCau] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLyHoCau] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLyHoCau] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [QLyHoCau]
GO
/****** Object:  StoredProcedure [dbo].[Config_DeleteAll]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[Config_DeleteAll] 
AS Delete  
from 
Config
Return










GO
/****** Object:  StoredProcedure [dbo].[Config_DeleteByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[Config_DeleteByPK] 
	(
@Id int
)
AS
Delete  from 
Config 
where 
[Id]=@Id


Return










GO
/****** Object:  StoredProcedure [dbo].[Config_GetAll]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Config_GetAll] 
AS
Select 
[Id],
[Name],
[Content],
[Status]

from 
Config
Return










GO
/****** Object:  StoredProcedure [dbo].[Config_GetByListId]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Config_GetByListId] 
	(
@Status int,
@id nvarchar(2000)
)
AS
SELECT	 
[Id],
[Name],
[Content],
[Status]

FROM 
Config  
WHERE 
[Status]=@Status and [Id] in (@id)


RETURN










GO
/****** Object:  StoredProcedure [dbo].[Config_GetByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Config_GetByPK] 
	(
@Id int
)
AS
SELECT	 
[Id],
[Name],
[Content],
[Status]

FROM 
Config  
WHERE 
[Id]=@Id


RETURN










GO
/****** Object:  StoredProcedure [dbo].[Config_GetByStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Config_GetByStatus] 
	(
@Status int
)
AS
SELECT	 
[Id],
[Name],
[Content],
[Status]

FROM 
Config  
WHERE 
[Status]=@Status


RETURN










GO
/****** Object:  StoredProcedure [dbo].[Config_Insert]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Config_Insert] 
	(
@Name nvarchar(50)=null,
@Content ntext=null,
@Status int=null
)
AS
Insert Into 
Config 
(
[Name],
[Content],
[Status]
) 
values(
@Name,
@Content,
@Status
)
RETURN Scope_identity()










GO
/****** Object:  StoredProcedure [dbo].[Config_UpdateByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Config_UpdateByPK] 
	(
@Id int,
@Name nvarchar(50)=null,
@Content ntext=null,
@Status int=null
)
AS
Update  Config Set 
[Name]=@Name,
[Content]=@Content,
[Status]=@Status
 
where 
[Id]=@Id


Return










GO
/****** Object:  StoredProcedure [dbo].[GroupAccount_GetByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GroupAccount_GetByPK] 
	(
@Id int
)
AS
SELECT	 
[Id],
[GroupAccountName],
[Authority],
[DateTimeCreate],
[DateTimeLastUpdate],
[Status]

FROM 
GroupAccount  
WHERE 
[Id]=@Id


RETURN







GO
/****** Object:  StoredProcedure [dbo].[GroupAccount_GetByStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GroupAccount_GetByStatus] 
	(
@Status int
)
AS
SELECT	 
ga.[Id],
ga.[GroupAccountName],
ga.[Authority],
ga.[DateTimeCreate],
ga.[DateTimeLastUpdate],
ga.[Status], (select count(*) from LoginAccount la where la.GroupAccountId = ga.Id and la.[Status]=1) as countAccount

FROM 
GroupAccount  ga
WHERE 
[Status]=@Status


RETURN







GO
/****** Object:  StoredProcedure [dbo].[GroupAccount_Insert]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GroupAccount_Insert] 
	(
@GroupAccountName nvarchar(500)=null,
@Authority ntext=null,
@DateTimeCreate datetime=null,
@DateTimeLastUpdate datetime=null,
@Status int=null
)
AS
Insert Into 
GroupAccount 
(
[GroupAccountName],
[Authority],
[DateTimeCreate],
[DateTimeLastUpdate],
[Status]
) 
values(
@GroupAccountName,
@Authority,
@DateTimeCreate,
@DateTimeLastUpdate,
@Status
)
RETURN Scope_identity()







GO
/****** Object:  StoredProcedure [dbo].[GroupAccount_UpdateByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GroupAccount_UpdateByPK] 
	(
@Id int,
@GroupAccountName nvarchar(500)=null,
@Authority ntext=null,
@DateTimeCreate datetime=null,
@DateTimeLastUpdate datetime=null,
@Status int=null
)
AS
Update  GroupAccount Set 
[GroupAccountName]=@GroupAccountName,
[Authority]=@Authority,
[DateTimeCreate]=@DateTimeCreate,
[DateTimeLastUpdate]=@DateTimeLastUpdate,
[Status]=@Status
 
where 
[Id]=@Id


Return







GO
/****** Object:  StoredProcedure [dbo].[GroupAccount_UpdateStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GroupAccount_UpdateStatus] 
	(
@Id int,
@Status int=null
)
AS
Update  GroupAccount Set 
[Status]=@Status
 
where 
[Id]=@Id


Return







GO
/****** Object:  StoredProcedure [dbo].[History_DeleteAll]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[History_DeleteAll] 
AS Delete  
from 
History
Return









GO
/****** Object:  StoredProcedure [dbo].[History_DeleteByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[History_DeleteByPK] 
	(
@Id int
)
AS
Delete  from 
History 
where 
[Id]=@Id


Return









GO
/****** Object:  StoredProcedure [dbo].[History_GetAll]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[History_GetAll] 
AS
Select 
[Id],
[Action],
[LoginAccountId],
[Content],
[DateTimeCreate]

from 
History
Return









GO
/****** Object:  StoredProcedure [dbo].[History_GetByPage]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[History_GetByPage] 
	(
@Page int,
@PageSize int
)
AS
select * from 
	(	
SELECT	 ROW_NUMBER()OVER ( ORDER BY h.[Id] desc) as Row , 
h.[Id],
h.[Action],
h.[LoginAccountId],(select LoginAccount.FullName from LoginAccount where LoginAccount.Id = h.LoginAccountId) as FullName,
h.[Content],
h.[DateTimeCreate], (select COUNT(h2.[Id]) from History h2) as countRow

FROM 
History h
)as tblTin
	where 
		tblTin.Row >(@Page - 1)*@PageSize
		AND tblTin.Row <= @Page*@PageSize



GO
/****** Object:  StoredProcedure [dbo].[History_GetByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[History_GetByPK] 
	(
@Id int
)
AS
SELECT	 
[Id],
[Action],
[LoginAccountId],
[Content],
[DateTimeCreate]

FROM 
History  
WHERE 
[Id]=@Id


RETURN









GO
/****** Object:  StoredProcedure [dbo].[History_Insert]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[History_Insert] 
	(
@Action nvarchar(50)=null,
@LoginAccountId int=null,
@Content nvarchar(1000),
@DateTimeCreate datetime=null
)
AS
Insert Into 
History 
(
[Action],
[LoginAccountId],
[Content],
[DateTimeCreate]
) 
values(
@Action,
@LoginAccountId,
@Content,
@DateTimeCreate
)
RETURN Scope_identity()









GO
/****** Object:  StoredProcedure [dbo].[History_UpdateByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[History_UpdateByPK] 
	(
@Id int,
@Action nvarchar(50)=null,
@LoginAccountId int=null,
@Content nvarchar(1000),
@DateTimeCreate datetime=null
)
AS
Update  History Set 
[Action]=@Action,
[LoginAccountId]=@LoginAccountId,
[Content]=@Content,
[DateTimeCreate]=@DateTimeCreate
 
where 
[Id]=@Id


Return









GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_CheckLogin]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LoginAccount_CheckLogin] 
	(
@Status int,
@UserName nvarchar(50),
@Password nvarchar(250)
)
AS
SELECT	 
la.[Id],
la.[GroupAccountId],
la.[UserName],
la.[Password],
la.[FullName],
la.[Phone],
la.[Email],
la.[Address],
la.[DateTimeCreate],
la.[LastUpdate],
la.[LastLogin],
la.[Status],
la.[AgencyId]

FROM 
LoginAccount  la
WHERE 
la.[Status]=@Status and la.UserName = @UserName and la.[Password] = @Password 


GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_DeleteAll]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[LoginAccount_DeleteAll] 
AS Delete  
from 
LoginAccount
Return










GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_DeleteByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[LoginAccount_DeleteByPK] 
	(
@Id int
)
AS
Delete  from 
LoginAccount 
where 
[Id]=@Id


Return










GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_GetAll]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[LoginAccount_GetAll] 
AS
Select 
[Id],
[GroupAccountId],
[UserName],
[Password],
[FullName],
[Phone],
[Email],
[Address],
[DateTimeCreate],
[LastUpdate],
[LastLogin],
[Status]

from 
LoginAccount
Return










GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_GetByGroupAccountId]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LoginAccount_GetByGroupAccountId] 
	(
@GroupAccountId int, @Status int, @AgencyId int
)
AS
SELECT	 
[Id],
[GroupAccountId],
[UserName],
[Password],
[FullName],
[Phone],
[Email],
[Address],
[DateTimeCreate],
[LastUpdate],
[LastLogin],
[Status], AgencyId

FROM 
LoginAccount  
WHERE 
[GroupAccountId]=@GroupAccountId and [Status]=@Status and AgencyId = @AgencyId






GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_GetByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LoginAccount_GetByPK] 
	(
@Id int
)
AS
SELECT	 
[Id],
[GroupAccountId],
[UserName],
[Password],
[FullName],
[Phone],
[Email],
[Address],
[DateTimeCreate],
[LastUpdate],
[LastLogin],
[Status],AgencyId

FROM 
LoginAccount  
WHERE 
[Id]=@Id




GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_GetByStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoginAccount_GetByStatus] 
	(
@Status int
)
AS
SELECT	 
la.[Id],
la.[GroupAccountId],
la.[UserName],
la.[Password],
la.[FullName],
la.[Phone],
la.[Email],
la.[Address],
la.[DateTimeCreate],
la.[LastUpdate],
la.[LastLogin],
la.[Status], ga.GroupAccountName as GroupAccountName, AgencyId

FROM 
LoginAccount  la
inner join GroupAccount ga on la.GroupAccountId=ga.Id
WHERE 
la.[Status]=@Status


RETURN










GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_Insert]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoginAccount_Insert] 
	(
@GroupAccountId int=null,
@UserName nvarchar(50)=null,
@Password nvarchar(250)=null,
@FullName nvarchar(50)=null,
@Phone nvarchar(50)=null,
@Email nvarchar(50)=null,
@Address nvarchar(500)=null,
@DateTimeCreate datetime=null,
@LastUpdate datetime=null,
@LastLogin datetime=null,
@Status int=null,
@AgencyId int = null
)
AS
Insert Into 
LoginAccount 
(
[GroupAccountId],
[UserName],
[Password],
[FullName],
[Phone],
[Email],
[Address],
[DateTimeCreate],
[LastUpdate],
[LastLogin],
[Status], AgencyId
) 
values(
@GroupAccountId,
@UserName,
@Password,
@FullName,
@Phone,
@Email,
@Address,
@DateTimeCreate,
@LastUpdate,
@LastLogin,
@Status, @AgencyId
)
RETURN Scope_identity()










GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_UpdateByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoginAccount_UpdateByPK] 
	(
@Id int,
@GroupAccountId int=null,
@UserName nvarchar(50)=null,
@Password nvarchar(250)=null,
@FullName nvarchar(50)=null,
@Phone nvarchar(50)=null,
@Email nvarchar(50)=null,
@Address nvarchar(500)=null,
@DateTimeCreate datetime=null,
@LastUpdate datetime=null,
@LastLogin datetime=null,
@Status int=null,
@AgencyId int = null
)
AS
Update  LoginAccount Set 
[GroupAccountId]=@GroupAccountId,
[UserName]=@UserName,
[Password]=@Password,
[FullName]=@FullName,
[Phone]=@Phone,
[Email]=@Email,
[Address]=@Address,
[DateTimeCreate]=@DateTimeCreate,
[LastUpdate]=@LastUpdate,
[LastLogin]=@LastLogin,
[Status]=@Status, AgencyId = @AgencyId
 
where 
[Id]=@Id


Return










GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_UpdatePasswordByPK]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[LoginAccount_UpdatePasswordByPK] 
	(
@Id int,
@Password nvarchar(250)=null
)
AS
Update  LoginAccount Set 
[Password]=@Password
where 
[Id]=@Id


Return










GO
/****** Object:  StoredProcedure [dbo].[LoginAccount_UpdateStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoginAccount_UpdateStatus] 
	(
@Id int,
@Status int
)
AS
Update  LoginAccount Set 
[Status]=@Status
where 
[Id]=@Id


Return










GO
/****** Object:  StoredProcedure [dbo].[Order_GetByPage]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Order_GetByPage] 
	(
@Status int,
@Page int,
@PageSize int
)
AS
select * from 
	(	
		SELECT	 ROW_NUMBER()OVER ( ORDER BY O.[DateTimeCreate] desc) as Row ,
		O.[Id]
      ,O.[Name]
      ,O.[Mobile]
      ,O.[Address]
      ,O.[Email]
      ,O.[DateTimeStart]
      ,O.[DateTimeEnd]
      ,O.[DateTimeCreate]
      ,O.[LoginAccountCheckin]
      ,O.[LoginAccountCheckout]
      ,O.[LoginAccountCreate]
      ,O.[Payments]
      ,O.[Promotion]
      ,O.[TotalPrice]
      ,O.[TotalPayment]
      ,O.[Note]
      ,O.[Value01]
      ,O.[Value02]
      ,O.[Value03]
      ,O.[Value04]
      ,O.[Value05]
      ,O.[Status]
		, (select COUNT(O2.[Id]) from [Order] O2 where O2.[Status]=@Status) as countRow

		FROM 
		dbo.[Order] O 
		WHERE 
		O.[Status]=@Status
)as tblTin
	where 
		tblTin.Row >(@Page - 1)*@PageSize
		AND tblTin.Row <= @Page*@PageSize

RETURN


































GO
/****** Object:  StoredProcedure [dbo].[Order_Insert]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[Order_Insert] 
	(
	@Name nvarchar(500) = null,
@Mobile nvarchar(500) = null,
@Address nvarchar(1000) = null,
@Email nvarchar(500) = null,
@DateTimeStart datetime = null,
@DateTimeEnd datetime = null,
@DateTimeCreate datetime = null,
@LoginAccountCheckin int = null,
@LoginAccountCheckout int = null,
@LoginAccountCreate int = null,
@Payments nvarchar(1000) = null,
@Promotion float = null,
@TotalPrice float = null,
@TotalPayment float = null,
@Note ntext = null,
@Value01 ntext = null,
@Value02 ntext = null,
@Value03 ntext = null,
@Value04 ntext = null,
@Value05 ntext = null,
@Status int = null
)
AS
Insert Into 
dbo.[Order]
(
[Name]
,[Mobile]
,[Address]
,[Email]
,[DateTimeStart]
,[DateTimeEnd]
,[DateTimeCreate]
,[LoginAccountCheckin]
,[LoginAccountCheckout]
,[LoginAccountCreate]
,[Payments]
,[Promotion]
,[TotalPrice]
,[TotalPayment]
,[Note]
,[Value01]
,[Value02]
,[Value03]
,[Value04]
,[Value05]
,[Status]
) 
values(
@Name,
@Mobile,
@Address,
@Email,
@DateTimeStart,
@DateTimeEnd,
@DateTimeCreate,
@LoginAccountCheckin,
@LoginAccountCheckout,
@LoginAccountCreate,
@Payments,
@Promotion,
@TotalPrice,
@TotalPayment,
@Note,
@Value01,
@Value02,
@Value03,
@Value04,
@Value05,
@Status
)
RETURN Scope_identity()







































GO
/****** Object:  StoredProcedure [dbo].[Order_UpdateStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Order_UpdateStatus] 
	(
@Id int,
@Status int=null
)
AS
Update  dbo.[Order] Set 
[Status]=@Status
 
where 
[Id]=@Id


Return







































GO
/****** Object:  StoredProcedure [dbo].[PriceBoard_GetByStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[PriceBoard_GetByStatus] 
	(
@Status int
)
AS
SELECT [Id]
      ,[TotalTimeStart]
      ,[TotalTimeEnd]
      ,[Price]
      ,[Note]
      ,[Value01]
      ,[Value02]
      ,[Value03]
      ,[Status]
  FROM [dbo].[PriceBoard]
WHERE 
[Status]=@Status


RETURN




GO
/****** Object:  StoredProcedure [dbo].[PriceBoard_Insert]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[PriceBoard_Insert] 
	(
	@TotalTimeStart float = null,
@TotalTimeEnd float = null,
@Price float = null,
@Note ntext=null,
@Value01 ntext=null,
@Value02 ntext=null,
@Value03 ntext=null,
@Status int=null
)
AS
Insert Into 
dbo.[PriceBoard]
(
[TotalTimeStart]
,[TotalTimeEnd]
,[Price]
,[Note]
,[Value01]
,[Value02]
,[Value03]
,[Status]
) 
values(
@TotalTimeStart
,@TotalTimeEnd
,@Price
,@Note
,@Value01
,@Value02
,@Value03
,@Status
)
RETURN Scope_identity()







































GO
/****** Object:  StoredProcedure [dbo].[PriceBoard_UpdateStatus]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[PriceBoard_UpdateStatus] 
	(
@Id int,
@Status int=null
)
AS
Update  dbo.[PriceBoard] Set 
[Status]=@Status
 
where 
[Id]=@Id


Return







































GO
/****** Object:  Table [dbo].[Config]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Config](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Content] [ntext] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupAccount]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupAccountName] [nvarchar](500) NULL,
	[Authority] [ntext] NULL,
	[DateTimeCreate] [datetime] NULL,
	[DateTimeLastUpdate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_GroupAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[History]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Action] [nvarchar](50) NULL,
	[LoginAccountId] [int] NULL,
	[Content] [nvarchar](1000) NOT NULL,
	[DateTimeCreate] [datetime] NULL,
 CONSTRAINT [PK_History] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoginAccount]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupAccountId] [int] NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](250) NULL,
	[FullName] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[DateTimeCreate] [datetime] NULL,
	[LastUpdate] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Status] [int] NULL,
	[AgencyId] [int] NULL,
 CONSTRAINT [PK_LoginAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Mobile] [nvarchar](500) NULL,
	[Address] [nvarchar](1000) NULL,
	[Email] [nvarchar](500) NULL,
	[DateTimeStart] [datetime] NULL,
	[DateTimeEnd] [datetime] NULL,
	[DateTimeCreate] [datetime] NULL,
	[LoginAccountCheckin] [int] NULL,
	[LoginAccountCheckout] [int] NULL,
	[LoginAccountCreate] [int] NULL,
	[Payments] [nvarchar](1000) NULL,
	[Promotion] [float] NULL,
	[TotalPrice] [float] NULL,
	[TotalPayment] [float] NULL,
	[Note] [ntext] NULL,
	[Value01] [ntext] NULL,
	[Value02] [ntext] NULL,
	[Value03] [ntext] NULL,
	[Value04] [ntext] NULL,
	[Value05] [ntext] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PriceBoard]    Script Date: 21/10/2022 3:49:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceBoard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TotalTimeStart] [float] NULL,
	[TotalTimeEnd] [float] NULL,
	[Price] [float] NULL,
	[Note] [ntext] NULL,
	[Value01] [ntext] NULL,
	[Value02] [ntext] NULL,
	[Value03] [ntext] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_PriceBoard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Config] ON 

INSERT [dbo].[Config] ([Id], [Name], [Content], [Status]) VALUES (5, N'Email', N'trithuctre.ql@gmail.com', 1)
INSERT [dbo].[Config] ([Id], [Name], [Content], [Status]) VALUES (6, N'Mobile', N'(+84) 0974.389.851', 1)
INSERT [dbo].[Config] ([Id], [Name], [Content], [Status]) VALUES (8, N'LinkPopupHomePage', N'https://hotrodaily.vn/', 1)
INSERT [dbo].[Config] ([Id], [Name], [Content], [Status]) VALUES (12, N'PopupHomePage', N'https://app.hotrodaily.vn/upload/popup.png', 1)
INSERT [dbo].[Config] ([Id], [Name], [Content], [Status]) VALUES (14, N'LinkReport', N'', 1)
SET IDENTITY_INSERT [dbo].[Config] OFF
SET IDENTITY_INSERT [dbo].[GroupAccount] ON 

INSERT [dbo].[GroupAccount] ([Id], [GroupAccountName], [Authority], [DateTimeCreate], [DateTimeLastUpdate], [Status]) VALUES (1, N'Quản lý đại lý', N'HomeManager,ManagerLoginAccount,ManagerAgency,ManagerBkavCard,ManagerOrder,ManagerCardWarehouse,ManagerPromotionOrder', CAST(0x0000A4ED011CB325 AS DateTime), CAST(0x0000AA06017AA66C AS DateTime), 1)
INSERT [dbo].[GroupAccount] ([Id], [GroupAccountName], [Authority], [DateTimeCreate], [DateTimeLastUpdate], [Status]) VALUES (2, N'Kỹ thuật', N'HomeTechnical,TechnicalLoginAccount,TechnicalAgency,TechnicalBkavCard', CAST(0x0000A51300ED62F1 AS DateTime), CAST(0x0000A85C015AFA04 AS DateTime), 1)
INSERT [dbo].[GroupAccount] ([Id], [GroupAccountName], [Authority], [DateTimeCreate], [DateTimeLastUpdate], [Status]) VALUES (3, N'Tổng đại lý', N'Home,SupperLoginAccount,SupperAgency,SupperBkavCard,SupperOrder,SupperCardWarehouse,SupperPromotionOrder,Config,Slide,News', CAST(0x0000A51300ED62F1 AS DateTime), CAST(0x0000A85C015B0ACA AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[GroupAccount] OFF
SET IDENTITY_INSERT [dbo].[LoginAccount] ON 

INSERT [dbo].[LoginAccount] ([Id], [GroupAccountId], [UserName], [Password], [FullName], [Phone], [Email], [Address], [DateTimeCreate], [LastUpdate], [LastLogin], [Status], [AgencyId]) VALUES (2, 3, N'admin', N'0cc175b9c0f1b6a831c399e269772661', N'Quản trị viên cao cấp', NULL, NULL, NULL, CAST(0x0000A41300000000 AS DateTime), CAST(0x0000AE49000BE6BB AS DateTime), CAST(0x0000AF2700A1AA1C AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[LoginAccount] OFF
ALTER TABLE [dbo].[History]  WITH CHECK ADD  CONSTRAINT [ FK_History_LoginAccount] FOREIGN KEY([LoginAccountId])
REFERENCES [dbo].[LoginAccount] ([Id])
GO
ALTER TABLE [dbo].[History] CHECK CONSTRAINT [ FK_History_LoginAccount]
GO
USE [master]
GO
ALTER DATABASE [QLyHoCau] SET  READ_WRITE 
GO
